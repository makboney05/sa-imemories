//
//  iUser.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/17/13.
//
//

#import <Foundation/Foundation.h>

@interface iUser : NSObject

@property (copy) NSString *firstName;
@property (copy) NSString *lastName;
@property (copy) NSString *profilePicture;
@property (copy) NSString *twitterID;
@property (copy) NSString *FBID;
@property (copy) NSString *contactNumber;
@property (copy) NSString *ID;
@property (copy) NSString *status;
@property (copy) NSString *email;
@property (copy) NSString *password;
@property (copy) NSString *DOB;
@property (copy) NSString *role;
@property (copy) NSString *createDate;

-(id)initWithDictionary:(NSDictionary *)aDictionary;
@end
