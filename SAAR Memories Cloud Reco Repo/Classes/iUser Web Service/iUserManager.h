//
//  iUserManager.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/16/13.
//
//

#import "iUser.h"
#import "Reachability.h"
#import <Foundation/Foundation.h>

#define ACTIVEEMAILUSER 1
#define ACTIVEFBUSER 1
#define ACTIVETWTERUSER 1

#define APPID @"1"
@protocol iUserManagerDelegateProtocol <NSObject>
@end

@interface iUserManager : NSObject

+ (iUser *) currentUser;

+ (iUserManager*) sharedInstance;

//send user name and password for login
- (NSString *)loginWithEmail:(NSString* )email andPassword:(NSString *)password;
- (NSString *)loginWithFaceBook:(NSString* )email;
- (NSString *)loginWithTwiteer:(NSString* )email;


- (NSString *)signUpUser:(iUser *)userModel;
@end
