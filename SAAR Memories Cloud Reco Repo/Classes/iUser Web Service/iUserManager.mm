
//
//  iUserManager.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/16/13.
//
//

#import "iUserManager.h"

#define BASEURL @"http://iuserservices.tutortoolbox.com/SAUserManagement.svc/"

@implementation iUserManager
static iUser *currenUser = nil;;
static iUserManager *sharedInstance = nil;

- (void)dealloc{

    currenUser = nil;
    [currenUser release];
    [super dealloc];
}
-(id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}


+ (iUserManager*) sharedInstance{

    @synchronized(self)
    {
		if (sharedInstance == nil)
        {
			sharedInstance = [[self alloc] init];
		}
	}
	return sharedInstance;
    
}

+ (iUser*) currentUser
{
    return currenUser;
}

- (BOOL) isInternetReachable {
	return ([[Reachability reachabilityForInternetConnection] currentReachabilityStatus] != NotReachable);
}

#pragma mark - 
#pragma mark - Class Methos
- (NSDate *)deserializeJsonDateString: (NSString *)jsonDateString
{
    
    NSInteger offset = [[NSTimeZone defaultTimeZone] secondsFromGMT]; //get number of seconds to add or subtract according to the client default time zone
    
    NSInteger startPosition = [jsonDateString rangeOfString:@"("].location + 1; //start of the date value
    
    NSTimeInterval unixTime = [[jsonDateString substringWithRange:NSMakeRange(startPosition, 13)] doubleValue] / 1000; //WCF will send 13 digit-long value for the time interval since 1970 (millisecond precision) whereas iOS works with 10 digit-long values (second precision), hence the divide by 1000
    
    NSDate *convertedDate = [[NSDate dateWithTimeIntervalSince1970:unixTime] dateByAddingTimeInterval:offset];
    return convertedDate;
}

#pragma mark - 
#pragma mark - Login Module
- (NSString *)performLoginWithUrl:(NSURL *)loginUrl{
    NSMutableURLRequest *request = [[NSMutableURLRequest requestWithURL:loginUrl cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0]retain];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/json" forHTTPHeaderField:@"AcceptType"];
    
    if ([self isInternetReachable]) {
        
        NSURLResponse *response = NULL;
        NSError *theError = NULL;
        NSData *theResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&theError];
        
        [request release];
        NSError* error;
        
        try{
            
            NSString *theResponseString = [[NSString alloc] initWithData:theResponseData encoding:NSUTF8StringEncoding];
            NSLog(@"response login=%@",theResponseString);
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:theResponseData options:kNilOptions error:&error];
            NSLog(@"response login json: %@", json);
            
            NSDictionary *userResultDic = [json objectForKey:@"GetValidUserResult"];
            id errorStr = [userResultDic objectForKey:@"Error"];
            [theResponseString release];
            NSLog(@"errorStr=%@",errorStr);
            if ([errorStr isEqual: [NSNull null]]) {
                NSLog(@"login successfull");
                currenUser = [[iUser alloc] initWithDictionary:userResultDic];
                NSLog(@"user name %@",currenUser.firstName);
                return @"Succesfull";
            }
            else
            {
                
                NSString *returnStr = (NSString *)errorStr;
                NSLog(@"login failed error %@",returnStr);
                return @"Login failed";
            }
        }
        
        catch(NSException *exc){
            return [exc description];
        }
    }else{
        [request release];
        return @"Internet not available";
    }
    [request release];
    return @"Login failed";
}
- (NSString *)loginWithEmail:(NSString* )email andPassword:(NSString *)password{

    NSMutableString *urlString = [[NSMutableString alloc] initWithFormat:@"%@GetValidUser?",BASEURL];
    
    /*int activeLogin = (int)[[NSUserDefaults standardUserDefaults] objectForKey:@"activeLogin"];
    activeLogin = 1;
    if (activeLogin == 1) {
        
        [urlString appendFormat:@"email=%@&password=%@",email,password];
        
    }else if (activeLogin == 2) {
    
        [urlString appendFormat:@"email=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"fbid"]];        
        
    }else{
    
        [urlString appendFormat:@"email=%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"fbid"]];
    }*/
    [urlString appendFormat:@"email=%@&password=%@&activelogin=%d",email,password,ACTIVEEMAILUSER];
    
    [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSLog(@"base url request %@", urlString);
    
    
    NSString *returnStr = [self performLoginWithUrl:[NSURL URLWithString:urlString]];
    [urlString release];
    return returnStr;
}

- (NSString *)loginWithFaceBook:(NSString* )email{

    NSMutableString *urlString = [[NSMutableString alloc] initWithFormat:@"%@GetValidUser?",BASEURL];
    [urlString appendFormat:@"email=%@&password=%@&activelogin=%d",email,@"",ACTIVEFBUSER];
    
    [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSLog(@"base url request %@", urlString);
    
    
    
    NSString *returnStr = [self performLoginWithUrl:[NSURL URLWithString:urlString]];
    [urlString release];
    return returnStr;
    
}
- (NSString *)loginWithTwiteer:(NSString* )email{

    NSMutableString *urlString = [[NSMutableString alloc] initWithFormat:@"%@GetValidUser?",BASEURL];
    [urlString appendFormat:@"email=%@&password=%@&activelogin=%d",email,@"",ACTIVETWTERUSER];
    
    [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSLog(@"base url request %@", urlString);
    
    
    
    NSString *returnStr = [self performLoginWithUrl:[NSURL URLWithString:urlString]];
    [urlString release];
    return returnStr;
}

#pragma mark - 
#pragma mark - Sign Up
- (NSString *)signUpUser:(iUser *)userModel{

    NSMutableString *urlString = [[NSMutableString alloc]initWithFormat:@"%@AddNewUser",BASEURL];
    //[urlString appendString:@"AddNewUser"];    
    [urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    NSLog(@"final url string=%@",urlString);
    
    NSMutableDictionary *attributeDic = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *userDic = [[NSMutableDictionary alloc] init];
    
    [attributeDic setObject:userModel.firstName forKey:@"FirstName"];
    [attributeDic setObject:userModel.lastName forKey:@"LastName"];
    [attributeDic setObject:userModel.email forKey:@"Email"];
    [attributeDic setObject:userModel.password forKey:@"Password"];
    [attributeDic setObject:[UIDevice currentDevice].uniqueIdentifier forKey:@"DeviceId"];
    [attributeDic setObject:[NSString stringWithFormat : @"/Date(%lld-0000)/",(long long)([[NSDate date] timeIntervalSince1970] * 1000)] forKey:@"CreateDate"];
    
    //[_params1 setObject:self.UploadFileResult forKey:@"ProfilePicture"];
    [userDic setObject:attributeDic forKey:@"user"];
    [userDic setObject:APPID forKey:@"applicationId"];
    //[userDic setObject:userModel.role forKey:@"role"];
    
    NSLog(@"dictionary str signup=%@",userDic);
    
    NSError* error;
    NSData *body = [NSJSONSerialization dataWithJSONObject:userDic options:NSJSONWritingPrettyPrinted error:&error];
    if (error) {
        //NSLog(@"error %@",[error description]);
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSMutableURLRequest *request = [[NSMutableURLRequest requestWithURL:url
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0]retain];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%d",[body length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:body];
    
    if (1) {
        //NSLog(@"net available");
        
        NSURLResponse *theResponse = NULL;
        NSError *theError = NULL;
        NSData *theResponseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&theResponse error:&theError];
        
        NSError* error;
        @try {
            
            NSString *theResponseString = [[NSString alloc] initWithData:theResponseData encoding:NSUTF8StringEncoding];
            NSLog(@"response=%@",theResponseString);
            
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:theResponseData //1
                                                                 options:kNilOptions
                                                                   error:&error];
            NSLog(@"json response: %@", json);
            
            NSDictionary *userResultDic = [json objectForKey:@"AddNewUserResult"];
            
            id errorStr = [userResultDic objectForKey:@"Error"];
            [theResponseString release];
            NSLog(@"errorStr=%@",errorStr);
            if ([errorStr isEqual: [NSNull null]]) {
            
                currenUser = [[iUser alloc] initWithDictionary:userResultDic];
                
                return @"Succesfull";
            }else{
            
                NSString *returnStr = (NSString *)errorStr;
                NSLog(@"login failed error %@",returnStr);
                return @"Signup failed";
            }
        }
        
        @catch (NSException * e) {
            return [e description];
        }
        
    }else{
        return @"Net not available";
    }
    return nil;
}
@end
