//
//  iUser.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/17/13.
//
//

#import "iUser.h"

@implementation iUser
@synthesize firstName;
@synthesize lastName;
@synthesize profilePicture;
@synthesize twitterID;
@synthesize FBID;
@synthesize contactNumber;
@synthesize ID;
@synthesize status;
@synthesize email;
@synthesize DOB;
@synthesize role;
@synthesize createDate;
@synthesize password;
-(id)initWithDictionary:(NSDictionary *)aDictionary
{
    self = [super init];
    if (self)
    {
        self.ID = [aDictionary objectForKey:@"Id"];
        self.firstName = [aDictionary objectForKey:@"FirstName"];
        self.lastName = [aDictionary objectForKey:@"LastName"];
        self.status = [aDictionary objectForKey:@"Status"];
        self.profilePicture = [aDictionary objectForKey:@"ProfilePicture"];
        self.twitterID = [aDictionary objectForKey:@"TwitterID"];
        self.FBID = [aDictionary objectForKey:@"FBID"];
        self.contactNumber = [aDictionary objectForKey:@"ContactNumber"];
    }
    
    return self;
}

- (void)dealloc{
    
    self.contactNumber = nil;
    self.ID = nil;
    self.twitterID = nil;
    self.FBID  = nil;
    self.firstName = nil;
    self.lastName = nil;
    self.status = nil;
    self.profilePicture = nil;
    self.email = nil;
    self.password = nil;
    self.DOB = nil;
    self.role = nil;
    self.createDate = nil;
    [super dealloc];
}

@end
