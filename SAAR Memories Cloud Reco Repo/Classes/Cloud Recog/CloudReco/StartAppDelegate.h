//
//  StartAppDelegate.h
//  SAAR Memories
//
//  Created by Boney's Macmini on 5/12/13.
//
//

#import <UIKit/UIKit.h>
#import "TemplateType.h"
#import "Field.h"
#import "Template.h"
#import "RegisteredEventManager.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKReverseGeocoder.h>
#import <AddressBook/AddressBook.h>

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface StartAppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate,RegisteredEventManagerDelegateProtocol>{

    TemplateType *_templateTypeEvent;
    TemplateType *_templateTypeAlbum;
    TemplateType *_templateTypeLocation;
    //TemplateType *_templateTypeInvestment;
    TemplateType *_templateTypePersonality;
    
    Field *fieldTitileTextField;
    Field *fieldUserNameTextField;
    Field *fieldUserPassTextField;
    Field *fieldWebSiteTextField;
    Field *fieldPinTextField;
    
    Field *fieldPersonAdjTextField;
    Field *fieldPersonTemperTextField;
    Field *fieldPersonFearTextField;
    Field *fieldPersonFriendTextField;
    Field *fieldPersonBirthDayTextField;
    
    Field *fieldbankNameTextField;
    Field *fieldAcountTypeTextField;
    Field *fieldAcountNOTextField;
    Field *fieldPhoneNumber;
    Field *fieldRoutingNOTextField;
    Field *fieldAccContctNoTextField;
    
    Field *fieldBrokarageCompanyTextField;
    Field *fieldAcountBalanceTextField;
    Field *fieldInterestRateTextField;
    Field *fieldSourceOfFund;
    
    Field *fieldLocationNameTextField;
    
    Field *fieldAttachment;
    
    Field *fieldAlbum;
    Field *fieldNote;
}
@property (strong, nonatomic) UITabBarController *tabBarController;

@property (strong, nonatomic) UINavigationController *navigationController;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic, retain) NSMutableArray *listedEvents;

@property (nonatomic, retain) CLPlacemark *place;
- (void)saveContext;
- (void)saveTemplateType;
- (void)saveField;
- (void)saveTemplate;
- (NSURL *)applicationDocumentsDirectory;
- (void)logedInSuccesfully;
@end
