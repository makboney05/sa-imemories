//
//  FetchedMemoryViewController.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/4/13.
//
//
#import "StartAppDelegate.h"
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Event.h"
#import "FieldValue.h"
@interface FetchedMemoryViewController : UITableViewController{

    StartAppDelegate *_appDelegate;
    NSArray *_dataList;
}
@property (retain) Event *fetchedEvent;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@end
