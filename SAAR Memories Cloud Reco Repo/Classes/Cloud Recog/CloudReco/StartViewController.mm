//
//  StartViewController.m
//  SAAR Memories
//
//  Created by Boney's Macmini on 5/12/13.
//
//

#import "StartViewController.h"

@interface StartViewController ()

@end

@implementation StartViewController
@synthesize cameraViewController = _cameraViewController;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _cameraViewController = [[CRParentViewController alloc] initWithNibName:nil bundle:nil];
    
    // need to set this so subsequent view controllers know the size
    _cameraViewController.arViewSize = self.view.bounds.size;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Event Handler
- (IBAction)goButton:(id)sender{

    [self.navigationController pushViewController:_cameraViewController animated:YES];
}
@end
