/*==============================================================================
 Copyright (c) 2012-2013 QUALCOMM Austria Research Center GmbH.
 All Rights Reserved.
 Qualcomm Confidential and Proprietary
 ==============================================================================*/

#import "CRParentViewController.h"
#import "CRViewController.h"
#import "CROverlayViewController.h"
#import "EAGLView.h"
#import "QCARHelper.h"
#import "CRQCARutils.h"
#import "BookWebDetailViewController.h"
#import "FetchedMemoryViewController.h"
#import "TargetOverlayView.h"
#import "AboutViewController.h"

@implementation CRParentViewController // subclass of ARParentViewController
@synthesize arViewSize = _arViewSize;
#pragma mark - Notifications

-(void)memoryButtonTapped:(NSNotification *)aNotification
{

    Event *aEvent = [CRQCARutils getInstance].lastScannedEvent;
    if (aEvent)
    {
        FetchedMemoryViewController *detailViewController = [[[FetchedMemoryViewController alloc] initWithNibName:@"FetchedMemoryViewController" bundle:nil] autorelease];
        detailViewController.fetchedEvent = aEvent;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}
-(void)officialButtonTapped:(NSNotification *)aNotification
{

    Event *aEvent = [CRQCARutils getInstance].lastScannedEvent;
    if (aEvent)
    {
        BookWebDetailViewController *bookWebDetailViewController = [[[BookWebDetailViewController alloc] initWithBook:aEvent] autorelease];
        bookWebDetailViewController.eventName = @"OFFICIAL";
        [self presentModalViewController:bookWebDetailViewController animated:YES];
    }
}
-(void)fbButtonTapped:(NSNotification *)aNotification
{
    Event *aEvent = [CRQCARutils getInstance].lastScannedEvent;
    if (aEvent)
    {
        BookWebDetailViewController *bookWebDetailViewController = [[[BookWebDetailViewController alloc] initWithBook:aEvent] autorelease];
        bookWebDetailViewController.eventName = @"FACEBOOK";
        [self presentModalViewController:bookWebDetailViewController animated:YES];
    }
}

#pragma mark - Public

// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
    [self createParentViewAndSplashContinuation];
    
    // Add the EAGLView
    arViewController = [[CRViewController alloc] init];
    
    // need to set size here to setup camera image size for AR
    arViewController.arViewSize = _arViewSize;
    [parentView addSubview:arViewController.view];
    
    // Hide the AR view so the parent view can be seen during start-up (the
    // parent view contains the splash continuation image on iPad and is empty
    // on iPhone and iPod)
    [arViewController.view setHidden:YES];
    
    // Create an auto-rotating overlay view and its view controller (used for
    // displaying UI objects, such as the camera control menu)
    CROverlayViewController *vsoVC = [[CROverlayViewController alloc] init];
    vsoVC.arViewController = (CRViewController *)arViewController;
    overlayViewController = vsoVC;
    [parentView addSubview: overlayViewController.view];
    
    self.view = parentView;
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    //[QCARHelper startDetection];fbButtonTapped
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fbButtonTapped:) name:@"kfbButtonTapped" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(officialButtonTapped:) name:@"kofficialButtonTapped" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(memoryButtonTapped:) name:@"kmemoryButtonTapped" object:nil];
}

- (void)viewDidDisappear:(BOOL)animated{

    [QCARHelper stopDetection];
    [super viewDidDisappear:animated];
    
}
- (void) dealloc
{
    [super dealloc];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //  Its superclass handles the overlay (2 taps) call
    [super touchesEnded:touches withEvent:event];
    
    UITouch* touch = [touches anyObject];
    
    if ([touch tapCount] == 1)
    {
        //  Get last scanned book
        Event *aEvent = [CRQCARutils getInstance].lastScannedEvent;
        
        if (aEvent)
        {
            CROverlayViewController *ovc = (CROverlayViewController *)overlayViewController;
            
            if (ovc.targetOverlayView.isHidden)
            {
                //  It's displaying the attached view
                CGPoint touchLocation = [touch locationInView:self.view];
                if ([(CRViewController *)arViewController isPointInsideAROverlay:touchLocation])
                {
                    //  Show Book WebView Detail
                    BookWebDetailViewController *bookWebDetailViewController = [[[BookWebDetailViewController alloc] initWithBook:aEvent] autorelease];
                    [self presentModalViewController:bookWebDetailViewController animated:YES];
                }
                
                //  We don't have to worry about the dettached view,
                //  TargetOverlayView has it's own touchesEnded call
            }
        }
    }    
}

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    //  Since CRParentViewController does not inherit from BaseViewController,
    //  we have to override this behavior
    
    BOOL retVal = [[CRQCARutils getInstance] shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
    return retVal;
}

#pragma mark -
#pragma mark Splash screen control
- (void)endSplash:(NSTimer*)theTimer
{
    // Poll to see if the camera video stream has started and if so remove the
    // splash screen
    [super endSplash:theTimer];
    
    if ([QCARutils getInstance].videoStreamStarted == YES)
    {
        // Create and show the about view
        [QCARHelper startDetection];
    }
}

@end
