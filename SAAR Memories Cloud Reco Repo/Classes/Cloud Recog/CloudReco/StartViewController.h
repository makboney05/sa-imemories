//
//  StartViewController.h
//  SAAR Memories
//
//  Created by Boney's Macmini on 5/12/13.
//
//

#import <UIKit/UIKit.h>
#import "CRParentViewController.h"
@interface StartViewController : UIViewController
- (IBAction)goButton:(id)sender;
@property (strong, nonatomic) CRParentViewController *cameraViewController;
@end
