//
//  FetchedMemoryViewController.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/4/13.
//
//

#import "FetchedMemoryViewController.h"

@interface FetchedMemoryViewController ()

@end

@implementation FetchedMemoryViewController
@synthesize fetchedEvent = _fetchedEvent;
@synthesize managedObjectContext = _managedObjectContext;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    _appDelegate = (StartAppDelegate *)[UIApplication sharedApplication].delegate;
    _managedObjectContext = _appDelegate.managedObjectContext;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	
	// NSSortDescriptor tells defines how to sort the fetched results
	NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"tags" ascending:YES];
	NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
	[fetchRequest setSortDescriptors:sortDescriptors];
	
	// fetchRequest needs to know what entity to fetch
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FieldValue" inManagedObjectContext:_managedObjectContext];
	[fetchRequest setEntity:entity];
	[sortDescriptors release];
	[sortDescriptor release];
	
	self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:_managedObjectContext sectionNameKeyPath:nil cacheName:@"Root"];
	
	[fetchRequest release];
    
    [self fetchResult];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    // Return the number of rows in the section.
    id  sectionInfo =
    [[_fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    FieldValue *info = [_fetchedResultsController objectAtIndexPath:indexPath];
    Template *templatee = info.thetemplate;
    NSLog(@"%@",templatee.templateType.name);
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:info.value];
    cell.detailTextLabel.text = templatee.templateType.name;
    cell.textLabel.text = [dic objectForKey:@"Name"];
    cell.imageView.image = [UIImage imageWithData:templatee.templateType.icon];
    //    if ([template.templateType.name isEqualToString:@"Album"]) {
    //        NSMutableArray *images = [dic objectForKey:@"IMAGES"];
    //        cell.imageView.image = [images objectAtIndex:0];
    //    }
    cell.detailTextLabel.numberOfLines = 5;
    cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 47.0f;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
}
#pragma mark -
#pragma mark Class Method
- (void)fetchResult{
    
    NSMutableArray *subpredicates = [NSMutableArray arrayWithCapacity:[self.fetchedEvent.tags count]];
    
    for (NSString *searchWord in self.fetchedEvent.tags) {
        [subpredicates addObject:[NSPredicate predicateWithFormat:
                                  @"tags CONTAINS[cd] %@ OR" // maybe speeds it up
                                  " tags MATCHES[cd] %@",
                                  searchWord, [NSString stringWithFormat:
                                               @".*\\b%@.*", searchWord]]];
        //[subpredicates addObject:[NSPredicate predicateWithFormat:@"tags contains[cd] %@",searchWord]];
    }
    
    
    self.fetchedResultsController.fetchRequest.predicate = [NSCompoundPredicate orPredicateWithSubpredicates:subpredicates];
    
    NSError *error = nil;
	if (![[self fetchedResultsController] performFetch:&error])
	{
		// Handle error
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
    
    
    NSLog(@"count %d",self.fetchedResultsController.fetchedObjects.count);
}
@end
