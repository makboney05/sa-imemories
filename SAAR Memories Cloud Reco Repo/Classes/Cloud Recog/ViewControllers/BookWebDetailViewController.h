/*==============================================================================
 Copyright (c) 2012-2013 QUALCOMM Austria Research Center GmbH.
 All Rights Reserved.
 Qualcomm Confidential and Proprietary
 ==============================================================================*/

#import <UIKit/UIKit.h>
#import "Event.h"
#import "BaseViewController.h"

@interface BookWebDetailViewController : BaseViewController
{
    IBOutlet UIWebView *webView;
    IBOutlet UINavigationBar *navigationBar;
    
    Event *event;
}

-(id)initWithBook:(Event *)aEvent;

@property (retain) Event *event;
@property (retain) NSString *eventName;
- (IBAction)doneButtonTapped:(id)sender;
@end
