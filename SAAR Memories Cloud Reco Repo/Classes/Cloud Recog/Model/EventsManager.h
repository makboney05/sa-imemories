//
//  EventsManager.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/26/13.
//
//

#import <Foundation/Foundation.h>
#import "EventsManagerDelegateProtocol.h"
@interface EventsManager : NSObject
{

    NSMutableSet *badTargets;
    NSString *thisTrackable;
    NSMutableData *eventInfo;
    id <EventsManagerDelegateProtocol> delegate;
}

@property (readwrite, nonatomic, setter = cancelNetworkOperations:) BOOL cancelNetworkOperation;
@property (readonly, nonatomic, getter = isNetworkOperationInProgress) BOOL networkOperationInProgress;

+(EventsManager *)sharedInstance;

-(void)eventWithJSONFilename:(NSString *)jsonFilename withDelegate:(id <EventsManagerDelegateProtocol>)aDelegate forTrackableID:(const char *)trackableID;
-(void)addBadTargetId:(const char*)aTargetId;
-(BOOL)isBadTarget:(const char*)aTargetId;
@end
