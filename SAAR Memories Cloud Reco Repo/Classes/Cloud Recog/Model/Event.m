//
//  Event.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/26/13.
//
//

#import "Event.h"

@implementation Event
@synthesize title = _title;
@synthesize location = _location;
@synthesize fbpage =_fbpage;
@synthesize officialpage = _officialpage;
@synthesize imageUrl = _imageUrl;
@synthesize description = _description;
@synthesize targetID = _targetID;
@synthesize tags = _tags;
-(id)initWithDictionary:(NSDictionary *)aDictionary
{
    self = [super init];
    if (self)
    {
        self.targetID = [aDictionary objectForKey:@"targetid"];
        self.description = [aDictionary objectForKey:@"description"];
        self.fbpage = [aDictionary objectForKey:@"fbpage"];
        self.officialpage = [aDictionary objectForKey:@"officialpage"];
        self.imageUrl = [aDictionary objectForKey:@"imageurl"];
        self.title = [aDictionary objectForKey:@"title"];
        self.location = [aDictionary objectForKey:@"location"];
        self.tags = (NSArray *)[aDictionary objectForKey:@"tags"];
    }
    
    return self;
}

- (void)dealloc{

    [super dealloc];
    [_title release];
    //[_location release];
    [_fbpage release];
    [_officialpage release];
    [_imageUrl release];
    [_description release];
    [_tags release];
}
@end
