//
//  EventDataParser.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/26/13.
//
//
//@property (copy) NSString *title;
//@property (copy) NSString *location;
//@property (copy) NSString *targetID;
//@property (copy) NSString *fbpage;
//@property (copy) NSString *officialpage;
//@property (copy) NSString *imageUrl;
//@property (copy) NSString *description;
#import "EventDataParser.h"
#import "Event.h"
@implementation EventDataParser
#define kEventParseTitleKey @"title"
#define kEventParseLocation @"location"
#define kEventParseFaceBookPage @"fbpage"
#define kEventParseOfficialPage @"officialpage"
#define kEventParseImageUrl @"imageUrl"
#define kEventParseDescription @"description"
#define kEventParseTargetID @"targetID"

#pragma mark - Private

+(NSString *)stringBetweenDoubleQuotes:(NSString *)val
{
    NSString *retVal = nil;
    
    //  Get index of first quote
    NSInteger initialIndex = [val rangeOfString:@"\""].location;
    
    //  Get index of last quote
    NSRange rangeForFinalIndex = NSMakeRange(initialIndex+1, [val length] - initialIndex - 1);
    NSInteger finalIndex = [val rangeOfString:@"\"" options:NSLiteralSearch range:rangeForFinalIndex].location;
    
    //  Get range of string between quotes
    NSRange substringRange = NSMakeRange(initialIndex+1, finalIndex - initialIndex -1);
    
    //  Get substring
    retVal = [val substringWithRange:substringRange];
    return retVal;
}

+(NSString *)keyFromLine:(NSString *)aJSONLine
{
    NSArray *elements = [aJSONLine componentsSeparatedByString:@":"];
    NSString *retVal = [EventDataParser stringBetweenDoubleQuotes:[elements objectAtIndex:0]];
    return retVal;
}

+(NSString *)valueFromLine:(NSString *)aJSONLine isURL:(BOOL)isURL
{
    NSArray *elements = [aJSONLine componentsSeparatedByString:@":"];
    NSString *stringToTransform = nil;
    
    if (isURL)
    {
        stringToTransform = [NSString stringWithFormat:@"%@:%@", [elements objectAtIndex:1], [elements objectAtIndex:2]];
    }
    else
    {
        stringToTransform = [elements objectAtIndex:1];
    }
    
    NSString *retVal = [EventDataParser stringBetweenDoubleQuotes:stringToTransform];
    return retVal;
}

#pragma mark - Public

+(NSDictionary *)parseData:(NSData *)dataToParse
{
    NSString *stringToParse = [[[NSString alloc] initWithData:dataToParse encoding:NSUTF8StringEncoding] autorelease];
    NSDictionary *retVal = [EventDataParser parseString:stringToParse];
    return retVal;
}

+(NSDictionary *)parseString:(NSString *)stringToParse
{
    NSMutableDictionary *tmpDict = [[[NSMutableDictionary alloc] init] autorelease];
    
    NSArray *jsonLines = [stringToParse componentsSeparatedByString:@","];
    
    for(NSString *line in jsonLines)
    {
        NSString *jsonKey = [EventDataParser keyFromLine:line];
        NSString *jsonValue = nil;
        
        if ([jsonKey isEqualToString:kEventParseFaceBookPage] ||
            [jsonKey isEqualToString:kEventParseOfficialPage] || [jsonKey isEqualToString:kEventParseImageUrl])
        {
            jsonValue = [EventDataParser valueFromLine:line isURL:YES];
        }
        else
        {
            jsonValue = [EventDataParser valueFromLine:line isURL:NO];
        }
        
        [tmpDict setObject:jsonValue forKey:jsonKey];
    }
    
    NSDictionary *retVal = [NSDictionary dictionaryWithDictionary:tmpDict];
    return retVal;
}
@end
