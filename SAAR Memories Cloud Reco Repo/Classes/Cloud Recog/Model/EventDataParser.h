//
//  EventDataParser.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/26/13.
//
//

#import <Foundation/Foundation.h>

@interface EventDataParser : NSObject
+(NSDictionary *)parseData:(NSData *)dataToParse;
+(NSDictionary *)parseString:(NSString *)stringToParse;
@end
