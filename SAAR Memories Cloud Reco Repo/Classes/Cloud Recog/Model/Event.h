//
//  Event.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/26/13.
//
//

#import <Foundation/Foundation.h>

@interface Event : NSObject

@property (copy) NSString *title;
@property (copy) NSString *location;
@property (copy) NSString *targetID;
@property (copy) NSString *fbpage;
@property (copy) NSString *officialpage;
@property (copy) NSString *imageUrl;
@property (copy) NSString *description;
@property (copy) NSArray *tags;


-(id)initWithDictionary:(NSDictionary *)aDictionary;
@end
