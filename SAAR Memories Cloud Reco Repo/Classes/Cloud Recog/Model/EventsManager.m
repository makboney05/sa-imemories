//
//  EventsManager.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/26/13.
//
//

#import "EventsManager.h"
#import "ImagesManager.h"
#import "EventsManagerDelegateProtocol.h"
#import "EventDataParser.h"
@implementation EventsManager
@synthesize cancelNetworkOperation, networkOperationInProgress;

#define EVENTSJSONURL @"https://copy.com/m9ZvsOXIFuCX/test"
static EventsManager *sharedInstance = nil;

#pragma mark - Public

-(void)eventWithJSONFilename:(NSString *)jsonFilename withDelegate:(id <EventsManagerDelegateProtocol>)aDelegate forTrackableID:(const char *)trackableID
{
    networkOperationInProgress = YES;
    
    //  Get URL
    NSString *anURLString = [NSString stringWithFormat:@"%@/%@", EVENTSJSONURL, jsonFilename];
    NSURL *anURL = [NSURL URLWithString:anURLString];
    NSLog(@"anURL %@",anURLString);
    [self infoForEventAtURL:anURL withDelegate:aDelegate forTrackableID:trackableID];
}

-(void)infoForEventAtURL:(NSURL* )url withDelegate:(id <EventsManagerDelegateProtocol>)aDelegate forTrackableID:(const char*)trackable
{
    // Store the delegate
    delegate = aDelegate;
    [delegate retain];
    
    // Store the trackable ID
    thisTrackable = [[NSString alloc] initWithCString:trackable encoding:NSASCIIStringEncoding];
    
    // Download the event info
    [self asyncDownloadInfoForEventAtURL:url];
}

-(void)asyncDownloadInfoForEventAtURL:(NSURL *)url
{
    // Download the info for this event
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] initWithURL:url] autorelease];
    [request setHTTPMethod:@"GET"];
    
    // Do not start the network operation immediately
    NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    
    // Use the run loop associated with the main thread
    [aConnection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    
    // Start the network operation
    [aConnection start];
}

-(void)addBadTargetId:(const char*)aTargetId
{
    NSString *tid = [NSString stringWithUTF8String:aTargetId];
    
    if (tid)
    {
        [badTargets addObject:tid];
    }
}

-(BOOL)isBadTarget:(const char*)aTargetId
{
    BOOL retVal = NO;
    NSString *tid = [NSString stringWithUTF8String:aTargetId];
    
    if (tid)
    {
        retVal = [badTargets containsObject:tid];
        
        if (retVal)
        {
            NSLog(@"#DEBUG bad target found");
        }
    }
    else
    {
        NSLog(@"#DEBUG error: could not convert const char * to NSString");
    }
    
    return retVal;
}

-(id)init
{
    self = [super init];
    if (self)
    {
        badTargets = [[NSMutableSet alloc] init];
    }
    return self;
}

+(EventsManager *)sharedInstance
{
	@synchronized(self)
    {
		if (sharedInstance == nil)
        {
			sharedInstance = [[self alloc] init];
		}
	}
	return sharedInstance;
}

-(BOOL)isNetworkOperationInProgress
{
    // The eventsManager or ImagesManager may have a network operation in
    // progress
    return networkOperationInProgress | [[ImagesManager sharedInstance] networkOperationInProgress] ? YES : NO;
}

-(void)cancelNetworkOperations:(BOOL)cancel
{
    // Set or clear the cancel flags, which will be checked in each network
    // callback
    
    // eventsManager (self)
    cancelNetworkOperation = cancel;
    
    // ImagesManager
    [[ImagesManager sharedInstance] setCancelNetworkOperation:cancel];
}

-(void)infoDownloadDidFinishWithEventData:(NSData *)eventData withConnection:(NSURLConnection *)connection
{
    Event *event = nil;
    
    if (eventData)
    {
        //  Given a NSData, parse the event to a dictionary and then convert it into a event object
        NSError *anError = nil;
        NSDictionary *eventDictionary = nil;
        
        //  Find out on runtime if the device can use NSJSONSerialization (iOS5 or later)
        NSString *className = @"NSJSONSerialization";
        Class class = NSClassFromString(className);
        
        if (!class)
        {
            //  Use custom eventDataParser.
            //
            //  IMPORTANT: eventDataParser is written to parse data specific to the CloudReco
            //  sample application and is not designed to be used in other applications.
            
            eventDictionary = [EventDataParser parseData:eventData];
            NSLog(@"#DEBUG Using custom JSONeventParser");
        }
        else
        {
            //  Use native JSON parser, NSJSONSerialization
            eventDictionary = [NSJSONSerialization JSONObjectWithData:eventData
                                                             options: NSJSONReadingMutableContainers
                                                               error: &anError];
            NSLog(@"#DEBUG Using NSJSONSerialization");
        }
        
        
        if (!eventDictionary)
        {
            NSLog(@"#DEBUG Error parsing JSON: %@", anError);
        }
        else
        {
            event = [[[Event alloc] initWithDictionary:eventDictionary] autorelease];
        }
    }
    
    //  Inform the delegate that the request has completed
    [delegate infoRequestDidFinishForEvent:event withTrackableID:[thisTrackable cStringUsingEncoding:NSASCIIStringEncoding] byCancelling:[self cancelNetworkOperation]];
    
    if (YES == [self cancelNetworkOperation])
    {
        // Inform the ImagesManager that the network operation has already been
        // cancelled (so its network operation will not be started and therefore
        // does not need to be cancelled)
        [self cancelNetworkOperations:NO];
    }
    
    // Release objects associated with the completed network operation
    [thisTrackable release];
    thisTrackable = nil;
    
    [delegate release];
    delegate = nil;
    
    [eventInfo release];
    eventInfo = nil;
    
    //  We don't need this connection reference anymore
    [connection release];
    
    networkOperationInProgress = NO;
}

#pragma mark NSURLConnectionDelegate
// *** These delegate methods are always called on the main thread ***
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self infoDownloadDidFinishWithEventData:nil withConnection:connection];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSData *eventData = nil;
    
    if (YES == [self cancelNetworkOperation])
    {
        // Cancel this connection
        [connection cancel];
    }
    else if (eventInfo)
    {
        eventData = [NSData dataWithData:eventInfo];
    }
    
    [self infoDownloadDidFinishWithEventData:eventData withConnection:connection];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (YES == [self cancelNetworkOperation])
    {
        // Cancel this connection
        [connection cancel];
        
        [self infoDownloadDidFinishWithEventData:nil withConnection:connection];
    }
    else
    {
        if (nil == eventInfo)
        {
            eventInfo = [[NSMutableData alloc] init];
        }
        
        [eventInfo appendData:data];
    }
}


#pragma mark Singleton overrides

+ (id)allocWithZone:(NSZone *)zone
{
    //  Overriding this method for singleton
    
	@synchronized(self)
    {
		if (sharedInstance == nil)
        {
			sharedInstance = [super allocWithZone:zone];
			return sharedInstance;
		}
	}
	return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    //  Overriding this method for singleton
	return self;
}

- (id)retain
{
    //  Overriding this method for singleton
    return self;
}

- (NSUInteger)retainCount
{
    //  Overriding this method for singleton
	return NSUIntegerMax;
}

- (oneway void)release
{
    //  Overriding this method for singleton
}

- (id)autorelease
{
    //  Overriding this method for singleton
	return self;
}
@end
