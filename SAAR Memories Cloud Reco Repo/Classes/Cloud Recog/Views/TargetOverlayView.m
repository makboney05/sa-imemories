/*==============================================================================
 Copyright (c) 2012-2013 QUALCOMM Austria Research Center GmbH.
 All Rights Reserved.
 Qualcomm Confidential and Proprietary
 ==============================================================================*/

#import "TargetOverlayView.h"
#import <QuartzCore/QuartzCore.h>
#import "Event.h"
#import "StarRatingView.h"
#import "ImagesManager.h"

@implementation TargetOverlayView

@synthesize titleLabel;
@synthesize descriptionLabel;
@synthesize priceContainerView;
@synthesize eventImageView;

- (void)dealloc {
    self.titleLabel = nil;
    self.priceContainerView = nil;
    self.eventImageView = nil;
    
    [super dealloc];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.priceContainerView.layer.affineTransform = CGAffineTransformMakeRotation(0.26f);
}

- (void)setEvent:(Event *)event {
    
    self.titleLabel.text = event.title;
    self.descriptionLabel.text = event.description;
    self.eventImageView.image = [[ImagesManager sharedInstance] cachedImageFromURL:event.imageUrl];
    
    //[self.starRatingView setRating:(int)book.ratingAverage];
    
    [self setNeedsDisplay];
}


#pragma mark Event Handler
- (IBAction)fbButtonTapped:(id)sender{

    [[NSNotificationCenter defaultCenter] postNotificationName:@"kfbButtonTapped" object:nil];
}
- (IBAction)memoryButtonTapped:(id)sender{

    [[NSNotificationCenter defaultCenter] postNotificationName:@"kmemoryButtonTapped" object:nil];
}
- (IBAction)officialButtonTapped:(id)sender{

    [[NSNotificationCenter defaultCenter] postNotificationName:@"kofficialButtonTapped" object:nil];
}
@end
