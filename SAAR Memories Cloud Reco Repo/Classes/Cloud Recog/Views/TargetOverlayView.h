/*==============================================================================
 Copyright (c) 2012-2013 QUALCOMM Austria Research Center GmbH.
 All Rights Reserved.
 Qualcomm Confidential and Proprietary
 ==============================================================================*/

#import <UIKit/UIKit.h>

@class Event;
@class StarRatingView;

@interface TargetOverlayView : UIView
{
}

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, retain) IBOutlet UIImageView *eventImageView;
@property (nonatomic, retain) IBOutlet UIView *priceContainerView;

- (IBAction)fbButtonTapped:(id)sender;
- (IBAction)memoryButtonTapped:(id)sender;
- (IBAction)officialButtonTapped:(id)sender;

- (void)setEvent:(Event *)event;

@end
