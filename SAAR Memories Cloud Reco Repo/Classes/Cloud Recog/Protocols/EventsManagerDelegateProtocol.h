//
//  EventsManagerDelegateProtocol.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/26/13.
//
//

#import <Foundation/Foundation.h>
#import "Event.h"
@protocol EventsManagerDelegateProtocol <NSObject>
-(void)infoRequestDidFinishForEvent:(Event *)theEvent withTrackableID:(const char*)trackable byCancelling:(BOOL)cancelled;
@end
