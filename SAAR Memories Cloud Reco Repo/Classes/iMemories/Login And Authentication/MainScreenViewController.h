//
//  MainScreenViewController.h
//  iMemories
//
//  Created by Lion Boney on 2/14/13.
//  Copyright (c) 2013 surroundapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StartAppDelegate.h"
@interface MainScreenViewController : UIViewController{

    StartAppDelegate *_delegate;
}
@property (nonatomic, strong) IBOutlet UIButton *loginBtn;
@property (nonatomic, strong) IBOutlet UIButton *registerBtn;

- (IBAction)loginBtnTapped:(id)sender;
- (IBAction)registerBtnTapped:(id)sender;
@end
