//
//  MainScreenViewController.m
//  iMemories
//
//  Created by Lion Boney on 2/14/13.
//  Copyright (c) 2013 surroundapps. All rights reserved.
//

#import "MainScreenViewController.h"

#import "LoginViewController.h"

#import "RegisterViewController.h"

@interface MainScreenViewController ()

@end

@implementation MainScreenViewController

- (void)fetchUserLocation{
    
    CLLocationManager *locationManager = [[[CLLocationManager alloc] init] autorelease];
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [locationManager startUpdatingLocation];
    
    CLGeocoder *geocoder = [[[CLGeocoder alloc] init] autorelease];
    
    [geocoder reverseGeocodeLocation:locationManager.location                        completionHandler:^(NSArray *placemarks, NSError *error) {
        
        dispatch_async(dispatch_get_main_queue(),^ {
            if (placemarks.count == 1) {
                
                _delegate.place = [placemarks objectAtIndex:0];                
            }
        });        
    }];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _delegate = (StartAppDelegate*)[UIApplication sharedApplication].delegate;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];

    CGPoint center4Login = CGPointMake(self.view.center.x, _loginBtn.center.y);
    CGPoint center4register = CGPointMake(self.view.center.x, _registerBtn.center.y);
    [UIView animateWithDuration:.5 animations:^{
        _loginBtn.center = center4Login;
        _registerBtn.center = center4register;
    } completion:^(BOOL finished){
    
        [self fetchUserLocation];
    }];
    
        
   
    
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 
#pragma mark Event Handler
- (IBAction)loginBtnTapped:(id)sender{

    LoginViewController *controller;
    if (IS_IPHONE_5) {
         controller= [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    }else{
     controller= [[LoginViewController alloc] initWithNibName:@"LoginViewController-Pre" bundle:nil];
    }
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}
- (IBAction)registerBtnTapped:(id)sender{

    RegisterViewController *controller;
    if (IS_IPHONE_5) {
        controller = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    }else{
        controller = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController-Pre" bundle:nil];
    }
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}
@end
