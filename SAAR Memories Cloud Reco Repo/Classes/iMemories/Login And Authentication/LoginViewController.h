//
//  LoginViewController.h
//  iMemories
//
//  Created by Lion Boney on 2/14/13.
//  Copyright (c) 2013 surroundapps. All rights reserved.
//
#import "Constants.h"
#import "iUserManager.h"
#import "MBProgressHUD.h"
#import <UIKit/UIKit.h>

#import "StartAppDelegate.h"

@interface LoginViewController : UIViewController<UITextFieldDelegate,MBProgressHUDDelegate>{

    StartAppDelegate *_delegate;
    MBProgressHUD *_hud;
    
    UIAlertView *_alertView;
}
@property (nonatomic, strong) IBOutlet UITextField *emailTxtField;
@property (nonatomic, strong) IBOutlet UITextField *passTxtField;

- (IBAction)loginBtnTapped:(id)sender;
- (IBAction)cancelBtnTapped:(id)sender;
@end
