//
//  SubMenuViewController.h
//  SAAR Memories Cloud Reco
//
//  Created by Lion Boney on 6/16/13.
//
//

#import "KYCircleMenu.h"
#import "MemoryVaultViewController.h"
@interface SubMenuViewController : KYCircleMenu
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@end
