//
//  LoginViewController.m
//  iMemories
//
//  Created by Lion Boney on 2/14/13.
//  Copyright (c) 2013 surroundapps. All rights reserved.
//

#import "LoginViewController.h"
#import "CircleMenuViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _delegate = (StartAppDelegate*)[UIApplication sharedApplication].delegate;
    
    _emailTxtField.clearButtonMode = UITextFieldViewModeWhileEditing;
    _passTxtField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [_emailTxtField setText:@"mahbub@gmail.com"];
    [_passTxtField setText:@"1"];

    [self.navigationController setNavigationBarHidden:NO];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar_log.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIBarButtonItem *logonBttn = [[UIBarButtonItem alloc] initWithTitle:@"Login" style:UIBarButtonItemStyleDone target:self action:@selector(loginBtnTapped:)];
    self.navigationItem.rightBarButtonItem = logonBttn;
    [logonBttn release];
    
    UIBarButtonItem *cancelBttn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelBtnTapped:)];
    self.navigationItem.leftBarButtonItem = cancelBttn;
    [cancelBttn release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [_passTxtField release];
    [_emailTxtField release];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    [super touchesBegan:touches withEvent:event];
    [_passTxtField resignFirstResponder];
    [_emailTxtField resignFirstResponder];
}
#pragma mark -
#pragma mark - Event Handler
- (IBAction)loginBtnTapped:(id)sender{
    if ([_emailTxtField.text length] == 0) {
        _alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Please enter a valid email." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [_alertView show];
        [_alertView release];
        [_emailTxtField becomeFirstResponder];
        return;
    }
    if ([_passTxtField.text length] == 0) {
        _alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Please enter a valid password." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [_alertView show];
        [_alertView release];
        [_passTxtField becomeFirstResponder];
        return;
    }
    if (!_hud) {
        _hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:_hud];
        _hud.userInteractionEnabled = NO;
        // Regiser for HUD callbacks so we can remove it from the window at the right time
        _hud.delegate = self;
    }
    __block NSString *success = @"";
    [_hud showAnimated:YES whileExecutingBlock:^{
    
        success = [[iUserManager sharedInstance] loginWithEmail:_emailTxtField.text andPassword:_passTxtField.text];
         
    } completionBlock:^{
        NSLog(@"type %@",[success class]);
        if ([success isEqualToString:@"Succesfull"]) {
            NSLog(@"current user id is %@ and name %@",[iUserManager currentUser].ID,[iUserManager currentUser].firstName);
            CircleMenuViewController * circleMenuViewController;
            circleMenuViewController = [CircleMenuViewController alloc];
            [circleMenuViewController initWithButtonCount:kKYCCircleMenuButtonsCount
                                                 menuSize:kKYCircleMenuSize
                                               buttonSize:kKYCircleMenuButtonSize
                                    buttonImageNameFormat:kKYICircleMenuButtonImageNameFormat
                                         centerButtonSize:kKYCircleMenuCenterButtonSize
                                    centerButtonImageName:kKYICircleMenuCenterButton
                          centerButtonBackgroundImageName:kKYICircleMenuCenterButtonBackground];
            [self.navigationController pushViewController:circleMenuViewController animated:YES];
            [circleMenuViewController release];
        }else{
        
            _alertView = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:success delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [_alertView show];
            [_alertView release];
        }
    }];
    
}
- (IBAction)cancelBtnTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark -
#pragma mark - UITextField Delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {

    //[_delegate logedInSuccesfully];
    //[self dismissViewControllerAnimated:NO completion:nil];
    if ([textField isEqual:_emailTxtField]) {
        [_passTxtField becomeFirstResponder];
    }else{
    
        [self loginBtnTapped:textField];
        [textField resignFirstResponder];
    }
    return YES;
}
@end
