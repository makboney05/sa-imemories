//
//  RegisterViewController.h
//  iMemories
//
//  Created by Lion Boney on 2/14/13.
//  Copyright (c) 2013 surroundapps. All rights reserved.
//

#import "iUser.h"
#import <UIKit/UIKit.h>
#import "iUserManager.h"
#import "SCNavigationBar.h"
#import "MBProgressHUD.h"
#import <QuartzCore/QuartzCore.h>

#define EMAILTXTTAG 101
#define PASSTXTTAG 102
#define FIRSTNAMETXTTAG 103
#define LASTNAMETXTTAG 104
#define PHNNAMETXTTAG 105
#define DOBTNAMETXTTAG 106
@interface RegisterViewController : UIViewController<MBProgressHUDDelegate>
{

    float _displacement;
    UITextField *_currentTextField;
    UITextField *_preveTextField;
    
    MBProgressHUD *_hud;
}
@property (nonatomic, strong) IBOutlet UIView *containerView;

@property (nonatomic, strong) IBOutlet UITextField *firstNameTxtField;
@property (nonatomic, strong) IBOutlet UITextField *lastNameTxtField;
@property (nonatomic, strong) IBOutlet UITextField *emailTxtField;
@property (nonatomic, strong) IBOutlet UITextField *passTxtField;
@property (nonatomic, strong) IBOutlet UITextField *DOBTxtField;
@property (nonatomic, strong) IBOutlet UITextField *contactTxtField;
@end
