//
//  SubMenuViewController.m
//  SAAR Memories Cloud Reco
//
//  Created by Lion Boney on 6/16/13.
//
//

#import "SubMenuViewController.h"

@interface SubMenuViewController ()

@end

@implementation SubMenuViewController
@synthesize managedObjectContext = _managedObjectContext;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidDisappear:(BOOL)animated{

    [super viewDidDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}
- (void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:YES];
    [self open];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Modify buttons' style in circle menu
    [self.navigationController setNavigationBarHidden:YES];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    for (UIButton * button in [self.menu subviews]){
        NSLog(@"tag %d",button.tag);
        [button setAlpha:.95f];
        if (button.tag == 1) {
            [button setImage:[UIImage imageNamed:@"KYICircleMenuButton04.png"] forState:UIControlStateNormal];
        }
        
        if (button.tag == 2) {
            [button setImage:[UIImage imageNamed:@"KYICircleMenuButton05.png"] forState:UIControlStateNormal];
        }
        
        if (button.tag == 3) {
            [button setImage:[UIImage imageNamed:@"KYICircleMenuButton06.png"] forState:UIControlStateNormal];
        }
        
        if (button.tag == 4) {
            [button setImage:[UIImage imageNamed:@"KYICircleMenuButton07.png"] forState:UIControlStateNormal];
        }
    }
    
 
    [self.centerButton setImage:[UIImage imageNamed:@"KYICircleMenuButton01.png"] forState:UIControlStateNormal];
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(self.view.bounds.size.width/2 - 15, self.view.bounds.size.height - 29, 29, 29)];
    [backButton addTarget:self action:@selector(backBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
    [backButton setBackgroundImage:[UIImage imageNamed:@"backbtn.png"] forState:UIControlStateNormal];
    [self.view addSubview:backButton];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - KYCircleMenu Button Action
- (IBAction)backBtnTapped:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
}
// Run button action depend on their tags:
//
// TAG:        1       1   2      1   2     1   2     1 2 3     1 2 3
//            \|/       \|/        \|/       \|/       \|/       \|/
// COUNT: 1) --|--  2) --|--   3) --|--  4) --|--  5) --|--  6) --|--
//            /|\       /|\        /|\       /|\       /|\       /|\
// TAG:                             3       3   4     4   5     4 5 6
//
- (void)runButtonActions:(id)sender {
    [super runButtonActions:sender];
    
    // Configure new view & push it with custom |pushViewController:| method
    /*UIViewController * viewController = [[UIViewController alloc] init];
     [viewController.view setBackgroundColor:[UIColor blackColor]];
     [viewController setTitle:[NSString stringWithFormat:@"View %d", [sender tag]]];
     // Use KYCircleMenu's |-pushViewController:| to push vc
     [self pushViewController:viewController];
     [viewController release];*/
    UIButton *senderButton = (UIButton*)sender;
    
    MemoryVaultViewController *controller = [[MemoryVaultViewController alloc] initWithNibName:@"MemoryVaultViewController" bundle:nil];
    
    switch (senderButton.tag) {
        case 1:
            controller.templateType = @"Album";
            break;

        case 2:
            controller.templateType = @"Event";
            break;
        case 3:
            controller.templateType = @"Location";
            break;
        case 4:
            controller.templateType = @"Personality";
            break;


        default:
            break;
    }
    controller.managedObjectContext = _managedObjectContext;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}
@end