//
//  RegisterViewController.m
//  iMemories
//
//  Created by Lion Boney on 2/14/13.
//  Copyright (c) 2013 lion boney. All rights reserved.
//

#import "RegisterViewController.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        /*UILabel *label = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont boldSystemFontOfSize:20.0];
        label.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        label.textAlignment = NSTextAlignmentCenter;
        // ^-Use UITextAlignmentCenter for older SDKs.
        label.textColor = [UIColor yellowColor]; // change this color
        self.navigationItem.titleView = label;
        label.text = NSLocalizedString(@"PageThreeTitle", @"");
        [label sizeToFit];*/
        
    }
    return self;
}
- (void)configureToolBarForKeyboard{
    
    UIBarButtonItem *previousBttn = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStyleDone target:self action:@selector(prevBtnTapped)];
    
    
    UIBarButtonItem *nextBttn = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(nextBtnTapped)];
    
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *doneBttn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneBtnTapped)];
    
    UIToolbar *keyBoardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    [keyBoardToolBar setItems:[NSArray arrayWithObjects:previousBttn,nextBttn,flexibleSpace,doneBttn, nil]];
    
    keyBoardToolBar.tintColor = [UIColor redColor];
    
    [self.emailTxtField setInputAccessoryView:keyBoardToolBar];
    [self.passTxtField setInputAccessoryView:keyBoardToolBar];
    [self.firstNameTxtField setInputAccessoryView:keyBoardToolBar];
    [self.lastNameTxtField setInputAccessoryView:keyBoardToolBar];
    [self.contactTxtField setInputAccessoryView:keyBoardToolBar];
    [self.DOBTxtField setInputAccessoryView:keyBoardToolBar];
    
    [previousBttn release];
    [nextBttn release];
    [flexibleSpace release];
    [keyBoardToolBar release];
    
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [super touchesEnded:touches withEvent:event];
    [_currentTextField resignFirstResponder];
    
    [UIView animateWithDuration:.4 animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController setNavigationBarHidden:NO];

    //[self.navigationController.navigationBar setTintColor:[UIColor colorWithWhite:0.878 alpha:1.000]];
    self.title = @"Register";
    
    self.containerView.layer.cornerRadius = 5;
    self.containerView.layer.masksToBounds = YES;
    self.containerView.layer.borderWidth = 2;
    self.containerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _displacement = 0.0f;
    [self configureToolBarForKeyboard];
    
    UIBarButtonItem *signUpBttn = [[UIBarButtonItem alloc] initWithTitle:@"Sign Up" style:UIBarButtonItemStyleDone target:self action:@selector(signUpBtnTapped)];
    self.navigationItem.rightBarButtonItem = signUpBttn;
    [signUpBttn release];
    
    UIBarButtonItem *cancelBttn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelBtnTapped)];
    self.navigationItem.leftBarButtonItem = cancelBttn;
    [cancelBttn release];
    
    SCNavigationBar *navBar = (SCNavigationBar *)self.navigationController.navigationBar;
    [navBar setTintColor:[UIColor colorWithWhite:0.878 alpha:1.000]];
    [navBar setBackgroundImage:[UIImage imageNamed:@"nav_bar_reg.png"] forBarMetrics:UIBarMetricsDefault];
    self.title = @"";
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleViewLayout{
    
    if (_currentTextField.tag > _preveTextField.tag) {
        _displacement -= 40.0f;
    }else _displacement += 40.0f;
    [UIView animateWithDuration:.4 animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, _displacement);
    }];
    
}

#pragma mark - 
#pragma mark - Button Event Handler
-(void)prevBtnTapped{
    
    if (_currentTextField.tag > EMAILTXTTAG) {
        int tag = _currentTextField.tag;
        UITextField *prevNextField = (UITextField *)[self.view viewWithTag:--tag];
        [prevNextField becomeFirstResponder];
        [self handleViewLayout];
    }
}

-(void)nextBtnTapped{
    NSLog(@"current tag %d",_currentTextField.tag);
    if (_currentTextField.tag < DOBTNAMETXTTAG) {
        int tag = _currentTextField.tag;
        UITextField *nextNextField = (UITextField *)[self.view viewWithTag:++tag];
        NSLog(@"next tag %d",nextNextField.tag);
        [nextNextField becomeFirstResponder];
        [self handleViewLayout];
        
    }
}
-(void)doneBtnTapped{

    [_currentTextField resignFirstResponder];
    [UIView animateWithDuration:.4 animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
}

- (void)cancelBtnTapped{

    [self.navigationController popViewControllerAnimated:YES];
}
-(void)signUpBtnTapped{

    [_currentTextField resignFirstResponder];
    iUser *userModel = [[iUser alloc] init];
    userModel.email = _emailTxtField.text;
    userModel.password = _passTxtField.text;
    userModel.firstName = _firstNameTxtField.text;
    userModel.lastName = _lastNameTxtField.text;
    userModel.DOB = _DOBTxtField.text;
    
    if (!_hud) {
        _hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
        [self.navigationController.view addSubview:_hud];
        _hud.userInteractionEnabled = NO;
        // Regiser for HUD callbacks so we can remove it from the window at the right time
        _hud.delegate = self;
    }
    __block NSString *success = @"";
    [_hud showAnimated:YES whileExecutingBlock:^{
        
        success = [[iUserManager sharedInstance] signUpUser:userModel];
        
    } completionBlock:^{
    
        if ([success isEqualToString:@"Succesfull"]) {
            NSLog(@"Successfull");
        }else{
        
            NSLog(@"Failed");
        }
    }];
    
}
#pragma mark -
#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    
    _currentTextField = textField;
    //[self handleViewLayout];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    _preveTextField = textField;
}
@end
