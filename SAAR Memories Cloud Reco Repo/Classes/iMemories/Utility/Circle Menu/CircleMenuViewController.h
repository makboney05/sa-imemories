//
//  CircleMenuViewController.h
//  KYCircleMenuDemo
//
//  Created by Kjuly on 7/18/12.
//  Copyright (c) 2012 Kjuly. All rights reserved.
//

#import "KYCircleMenu.h"
#import "MemoryVaultViewController.h"
#import "RegisterTargetViewController.h"
#import "StartAppDelegate.h"
#import "CRParentViewController.h"
#import "SubMenuViewController.h"
#import "Constants.h"
@interface CircleMenuViewController : KYCircleMenu
{

    StartAppDelegate *_delegate;
}
@property (strong, nonatomic) CRParentViewController *cameraViewController;
@end
