//
//  CircleMenuViewController.m
//  KYCircleMenuDemo
//
//  Created by Kjuly on 7/18/12.
//  Copyright (c) 2012 Kjuly. All rights reserved.
//

#import "CircleMenuViewController.h"

@implementation CircleMenuViewController
@synthesize cameraViewController = _cameraViewController;
- (void)dealloc {
  [super dealloc];
}

- (id)init {
  if (self = [super init])
    [self setTitle:@"KYCircleMenu"];
  return self;
}

- (void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}
- (void)viewDidLoad {
  [super viewDidLoad];
  // Modify buttons' style in circle menu
    
  [self.view setBackgroundColor:[UIColor whiteColor]];
    
  for (UIButton * button in [self.menu subviews])
    [button setAlpha:.95f];
    
    _delegate = (StartAppDelegate*)[UIApplication sharedApplication].delegate;
    
    _cameraViewController = [[CRParentViewController alloc] initWithNibName:nil bundle:nil];
    
    // need to set this so subsequent view controllers know the size
    _cameraViewController.arViewSize = self.view.bounds.size;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(self.view.bounds.size.width/2 - 15, self.view.bounds.size.height - 40, 29, 38)];
    [backButton addTarget:self action:@selector(logOutBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    [backButton.titleLabel setFont:[UIFont fontWithName:@"Arial" size:10.0f]];
    [backButton setBackgroundImage:[UIImage imageNamed:@"logoutbtn.png"] forState:UIControlStateNormal];
    [backButton setBackgroundImage:[UIImage imageNamed:@""] forState:UIControlStateHighlighted];
    [self.view addSubview:backButton];
    
    [self.centerButton setImage:[UIImage imageNamed:@"KYICircleMenuCenterButton"] forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - KYCircleMenu Button Action
-(IBAction)logOutBtnTapped:(id)sender{

    [self.navigationController popToRootViewControllerAnimated:YES];
}
// Run button action depend on their tags:
//
// TAG:        1       1   2      1   2     1   2     1 2 3     1 2 3
//            \|/       \|/        \|/       \|/       \|/       \|/
// COUNT: 1) --|--  2) --|--   3) --|--  4) --|--  5) --|--  6) --|--
//            /|\       /|\        /|\       /|\       /|\       /|\
// TAG:                             3       3   4     4   5     4 5 6
//
- (void)runButtonActions:(id)sender {
  [super runButtonActions:sender];
  
  // Configure new view & push it with custom |pushViewController:| method
  /*UIViewController * viewController = [[UIViewController alloc] init];
  [viewController.view setBackgroundColor:[UIColor blackColor]];
  [viewController setTitle:[NSString stringWithFormat:@"View %d", [sender tag]]];
  // Use KYCircleMenu's |-pushViewController:| to push vc
  [self pushViewController:viewController];
  [viewController release];*/
    UIButton *senderButton = (UIButton*)sender;
    
    if (senderButton.tag == 1) {
        /*MemoryVaultViewController *controller = [[MemoryVaultViewController alloc] initWithNibName:@"MemoryVaultViewController" bundle:nil];
        controller.managedObjectContext = _delegate.managedObjectContext;
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];*/
        SubMenuViewController *controller = [SubMenuViewController alloc];
        [controller initWithButtonCount:4
                                             menuSize:kKYCircleMenuSize
                                           buttonSize:kKYCircleMenuButtonSize
                                buttonImageNameFormat:kKYICircleMenuButtonImageNameFormat
                                     centerButtonSize:kKYCircleMenuCenterButtonSize
                                centerButtonImageName:kKYICircleMenuCenterButton
                      centerButtonBackgroundImageName:kKYICircleMenuCenterButtonBackground];
        [self.navigationController pushViewController:controller animated:YES];
        controller.managedObjectContext = _delegate.managedObjectContext;
        self.navigationController.navigationBarHidden = YES;
        [controller release];
    }else if (senderButton.tag == 2) {
        RegisterTargetViewController *rgstrCOntroller;
        if (IS_IPHONE_5) {
            rgstrCOntroller = [[RegisterTargetViewController alloc] initWithNibName:@"RegisterTargetViewController" bundle:nil];
        }else{
        rgstrCOntroller = [[RegisterTargetViewController alloc] initWithNibName:@"RegisterTargetViewController-Pre" bundle:nil];
        }
        [self.navigationController pushViewController:rgstrCOntroller animated:YES];
        [rgstrCOntroller release];
        [self.navigationController setNavigationBarHidden:NO];
        
        return;
    }else if (senderButton.tag == 3) {
        [self.navigationController pushViewController:_cameraViewController animated:YES];
    }
    self.title = @"Menu";
}

@end
