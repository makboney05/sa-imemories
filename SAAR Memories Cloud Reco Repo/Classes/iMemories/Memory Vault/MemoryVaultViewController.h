//
//  MemoryVaultViewController.h
//  iMemories
//
//  Created by Lion Boney on 2/14/13.
//  Copyright (c) 2013 surroundapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WEPopoverController.h"
#import "CategoryViewController.h"
#import <CoreData/CoreData.h>
#import "CRParentViewController.h"
#import "AlbumEntryViewController.h"
#import "PersonalityEntryViewController.h"
#import "Photos.h"
#import "SCNavigationBar.h"

@interface MemoryVaultViewController : UIViewController<PassTypeTomemoryVaultEntry,WEPopoverControllerDelegate, UIPopoverControllerDelegate,NSFetchedResultsControllerDelegate,UIAlertViewDelegate>
{
    Class popoverClass;
    CategoryViewController *contentViewController;
    
    UIAlertView *_alertView;
    FieldValue *_fieldValToDelete;
    
}
@property (nonatomic, retain) WEPopoverController *popoverController;
@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain) IBOutlet UITableView *memoryVaultTblView;
@property (strong, nonatomic) CRParentViewController *cameraViewController;
@property (nonatomic, retain) NSString *templateType;
- (void)reloadView;
@end
