//
//  AlbumEntryViewController.h
//  SAAR Memories
//
//  Created by Boney's Macmini on 5/15/13.
//
//

#import <UIKit/UIKit.h>
#import "StartAppDelegate.h"
#import "FieldValue.h"
#import "AddAlbumViewController.h"
#import "Template.h"
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MKReverseGeocoder.h>
#import <AddressBook/AddressBook.h>
#import <QuartzCore/QuartzCore.h>
#import "AddAlbumViewController.h"
#import "SampleViewController.h"
#import "RegisteredEvent.h"
@interface AlbumEntryViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>
{

    StartAppDelegate *_appDelegate;
    NSMutableIndexSet *_expandedSections;
    
    NSMutableArray *_selectedIndexes;
}
@property (nonatomic, strong) IBOutlet UITextField *albumNameTxtField;
@property (nonatomic, strong) IBOutlet UITextField *locationTxtField;
@property (nonatomic, strong) IBOutlet UITextView *noteTxtView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *locActivityView;
@property (nonatomic, strong) IBOutlet UIView *borderView;
@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UISegmentedControl *segmentControl;
@property (nonatomic, retain) FieldValue *fieldValueModel;
@property (nonatomic, retain) Template *selectedTemplate;
@property (nonatomic, retain) CLLocation *location;

- (IBAction)saveButtonTapped:(id)sender;
- (IBAction)cancelButtonTapped:(id)sender;
@end
