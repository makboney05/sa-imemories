//
//  AddAlbumViewController.h
//  SAAR Memories
//
//  Created by Boney's Macmini on 5/15/13.
//
//

#import <UIKit/UIKit.h>
#import "KTThumbsViewController.h"
#import "PhotoPickerController.h"
#import "Photos.h"
#import "FieldValue.h"
#import "Template.h"
#import "AlbumEntryViewController.h"

@class AlbumEntryViewController;
@class Photos;

@interface AddAlbumViewController : KTThumbsViewController <PhotoPickerControllerDelegate, PhotosDelegate>
{

    PhotoPickerController *photoPicker_;
    Photos *myPhotos_;
    UIActivityIndicatorView *activityIndicatorView_;
    AlbumEntryViewController *_entryController;
}

@property (nonatomic, retain) NSString *albumName;
@property (nonatomic, retain) Template *selectedTemplate;
@property (nonatomic, retain) FieldValue *selectedFieldValue;

- (void)cancelEvent;
- (void)saveEvent:(NSString *)albumName;
@end
