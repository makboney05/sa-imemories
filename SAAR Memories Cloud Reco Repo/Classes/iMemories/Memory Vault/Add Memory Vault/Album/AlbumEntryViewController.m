//
//  AlbumEntryViewController.m
//  SAAR Memories
//
//  Created by Boney's Macmini on 5/15/13.
//
//

#import "AlbumEntryViewController.h"
#define kGeoCodingString @"http://maps.google.com/maps/geo?q=%f,%f&output=csv" //define this at top

@interface AlbumEntryViewController ()

- (NSString *)getTagValue;
- (NSString *)getType;
@end

@implementation AlbumEntryViewController
@synthesize fieldValueModel = _fieldValueModel;
@synthesize selectedTemplate = _selectedTemplate;
@synthesize location = _location;
@synthesize tableView = _tableView;
@synthesize segmentControl = _segmentControl;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        if (!_expandedSections)
        {
            _expandedSections = [[NSMutableIndexSet alloc] init];
        }
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    [super touchesBegan:touches withEvent:event];
    [_albumNameTxtField resignFirstResponder];
    [_noteTxtView resignFirstResponder];
    [_locationTxtField resignFirstResponder];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _appDelegate = (StartAppDelegate *)[UIApplication sharedApplication].delegate;
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave
                                                                               target:self
                                                                               action:@selector(saveButtonTapped:)];
    [[self navigationItem] setRightBarButtonItem:saveButton];
    [saveButton release];
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = _borderView.bounds;
    
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:0.408 alpha:1.000] CGColor], (id)[[UIColor colorWithWhite:0.122 alpha:1.000] CGColor], nil];
    [_borderView.layer insertSublayer:gradient atIndex:0];
    
    _albumNameTxtField.placeholder = [_selectedTemplate.templateType.name stringByAppendingFormat:@" Name"];
    [_albumNameTxtField becomeFirstResponder];
    [_locActivityView setHidesWhenStopped:YES];
    
    if (_fieldValueModel) {
         NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:_fieldValueModel.value];
        _albumNameTxtField.text = [dic objectForKey:@"Name"];
        _albumNameTxtField.enabled = NO;
        _locationTxtField.text = [dic objectForKey:@"Location"];
        _noteTxtView.text = [dic objectForKey:@"Note"];
        //self.title = _albumNameTxtField.text;
    }else{
        _locationTxtField.text = _appDelegate.place.locality;
        //self.title = _selectedTemplate.templateType.name;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [_location release];
    _location = nil;
    
    _albumNameTxtField = nil;
    _locationTxtField = nil;
    _locActivityView = nil;
    _borderView = nil;
    
    _selectedTemplate = nil;
    _fieldValueModel = nil;
}

#pragma mark -
#pragma mark Button Event Handler
- (IBAction)cancelButtonTapped:(id)sender{
    [UIView animateWithDuration:.5 animations:^{
        self.view.frame = CGRectMake(10, -(self.view.frame.size.height + 44), self.view.frame.size.width, self.view.frame.size.height);
        
    } completion:^(BOOL finished) {
        [_albumNameTxtField resignFirstResponder];
        [_locationTxtField resignFirstResponder];
        [_noteTxtView resignFirstResponder];
        [[(AddAlbumViewController *)[self.view superview] nextResponder] performSelector:@selector(cancelEvent)];
    }];
}

- (IBAction)saveButtonTapped:(id)sender{
    if (!_fieldValueModel) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:_albumNameTxtField.text forKey:@"Name"];
        [dic setObject:_noteTxtView.text forKey:@"Note"];
        [dic setObject:_locationTxtField.text forKey:@"Location"];
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
    
        FieldValue *fieldValue = (FieldValue *)[NSEntityDescription insertNewObjectForEntityForName:@"FieldValue" inManagedObjectContext:_appDelegate.managedObjectContext];
        [fieldValue setValue:data];
        [fieldValue setCreatedBy:[@"admin" uppercaseString]];
        [fieldValue setUpdatedBy:[@"admin" uppercaseString]];
        [fieldValue setCreatedDate:[NSDate date]];
        [fieldValue setUpdatedDate:[NSDate date]];
        [fieldValue setTags:[self getTagValue]];
        [fieldValue setType:[self getType]];
        [fieldValue setThetemplate:_selectedTemplate];
        
        [dic release];
        NSError *error = nil;
        if (![_appDelegate.managedObjectContext save:&error]) {
            //Handle Error
            
            // TO DO
            NSLog(@"Handle Error");
        }else{
            
            [UIView animateWithDuration:.5 animations:^{
                self.view.frame = CGRectMake(10, -(self.view.frame.size.height + 44), self.view.frame.size.width, self.view.frame.size.height);
                
            } completion:^(BOOL finished) {
                [_albumNameTxtField resignFirstResponder];
                [_locationTxtField resignFirstResponder];
                [_noteTxtView resignFirstResponder];
                [[(AddAlbumViewController *)[self.view superview] nextResponder] performSelector:@selector(saveEvent:) withObject:_albumNameTxtField.text];
            }];
        }
    }else{
        // TO DO    
    }
    
}

#pragma mark - Expanding

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    if (section == 0) return YES;
    
    return NO;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([_expandedSections containsIndex:section])
        {
            NSLog(@"total event count %d",_appDelegate.listedEvents.count);
            return [_appDelegate.listedEvents count] + 1;
        }

        return 1;
    }

    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    // Configure the cell...
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            cell.textLabel.text = @"Register This Memory With Event"; 
            UIFont *myFont = [ UIFont systemFontOfSize:17.0f];
            cell.textLabel.font  = myFont;
            
            if ([_expandedSections containsIndex:indexPath.section])
            {
                cell.textLabel.textColor = [UIColor grayColor];
            }
            else
            {
                cell.textLabel.textColor = [UIColor lightGrayColor];
            }
        }
        else
        {
            // all other rows
            RegisteredEvent *Revent = [_appDelegate.listedEvents objectAtIndex:indexPath.row - 1];
            cell.textLabel.text = Revent.title;
            cell.textLabel.textColor = [UIColor blackColor];
            UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 15.0 ];
            cell.textLabel.font  = myFont;
            cell.accessoryView = nil;
            cell.textLabel.adjustsFontSizeToFitWidth = YES;            
        }
    }
    else
    {
        cell.accessoryView = nil;
        cell.textLabel.text = @"Normal Cell";        
    }
    
    return cell;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 34;
}
/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_albumNameTxtField resignFirstResponder];
    [_locActivityView resignFirstResponder];
    [_noteTxtView resignFirstResponder];
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            [self.tableView beginUpdates];
            
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [_expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded)
            {
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [_expandedSections removeIndex:section];
                
            }
            else
            {
                [_expandedSections addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            if (currentlyExpanded)
            {
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                
            }
            else
            {
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
            }
            
            [self.tableView endUpdates];
        }else{
            if (!_selectedIndexes) {
                _selectedIndexes = [[NSMutableArray alloc] init];
            }
            UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
            
            if ([selectedCell accessoryType] == UITableViewCellAccessoryNone) {
                [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
                [_selectedIndexes addObject:[NSNumber numberWithInt:indexPath.row]];
            } else {
                [selectedCell setAccessoryType:UITableViewCellAccessoryNone];
                [_selectedIndexes removeObject:[NSNumber numberWithInt:indexPath.row]];
            }
            
            [tableView deselectRowAtIndexPath:indexPath animated:NO];

        }
    }
}

#pragma mark -
#pragma mark Location Class Methods
- (NSString *)getType{

    switch (_segmentControl.selectedSegmentIndex) {
        case 0:
            return @"PUBLIC";
            break;
            
        case 1:
            return @"PRIVATE";
            break;
            
        case 2:
            return @"SHARED";
            break;
            
        default: return @"PUBLIC";
            break;
    }
}
- (NSString *)getTagValue{
    NSString *tagValue = @"";
    for (NSNumber *index in _selectedIndexes) {
        RegisteredEvent *evnt = [_appDelegate.listedEvents objectAtIndex:index.intValue];
        tagValue = [tagValue stringByAppendingFormat:@"%@,",evnt.title];
    }
    return tagValue;
}
@end
