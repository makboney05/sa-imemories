//
//  AddAlbumViewController.m
//  SAAR Memories
//
//  Created by Boney's Macmini on 5/15/13.
//
//

#import "AddAlbumViewController.h"

@interface AddAlbumViewController ()
- (UIActivityIndicatorView *)activityIndicator;
- (void)showActivityIndicator;
- (void)hideActivityIndicator;

@end

@implementation AddAlbumViewController
@synthesize albumName = _albumName;
@synthesize selectedFieldValue = _selectedFieldValue;
@synthesize selectedTemplate = _selectedTemplate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera
                                                                               target:self
                                                                               action:@selector(addPhoto)];
    [[self navigationItem] setRightBarButtonItem:addButton];
    [addButton release];
    
    if (_selectedFieldValue) {
        
        if (myPhotos_ == nil) {
            myPhotos_ = [[Photos alloc] initWithFolderName:_albumName];
            [myPhotos_ setDelegate:self];
        }
        [self setDataSource:myPhotos_];
        //self.title = _albumName;
    }else{
    
        //self.title = _selectedTemplate.templateType.name;
        _entryController = [[AlbumEntryViewController alloc] initWithNibName:@"AlbumEntryViewController" bundle:nil];
        _entryController.selectedTemplate = _selectedTemplate;
        [self.view addSubview:_entryController.view];
        _entryController.view.frame = CGRectMake(10, self.view.bounds.size.height, _entryController.view.frame.size.width, _entryController.view.frame.size.height);// somewhere offscreen, in the direction you want it to appear from
        [UIView animateWithDuration:.5
                         animations:^{
                             _entryController.view.frame = CGRectMake(10, 10, _entryController.view.frame.size.width, _entryController.view.frame.size.height);
                         }];
        self.navigationController.navigationBar.userInteractionEnabled = NO;
    }
    
}
- (void)willLoadThumbs
{
    [self showActivityIndicator];
}

- (void)didLoadThumbs
{
    [self hideActivityIndicator];
}


#pragma mark -
#pragma mark Activity Indicator

- (UIActivityIndicatorView *)activityIndicator
{
    if (activityIndicatorView_) {
        return activityIndicatorView_;
    }
    
    activityIndicatorView_ = [[UIActivityIndicatorView alloc]
                              initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [activityIndicatorView_ setCenter:self.view.center];
    [[self view] addSubview:activityIndicatorView_];
    
    return activityIndicatorView_;
}

- (void)showActivityIndicator
{
    [[self activityIndicator] startAnimating];
}

- (void)hideActivityIndicator
{
    [[self activityIndicator] stopAnimating];
}


#pragma mark -
#pragma mark Actions

- (void)addPhoto
{
    if (!photoPicker_) {
        photoPicker_ = [[PhotoPickerController alloc] initWithDelegate:self];
    }
    [photoPicker_ show];
}
- (void)cancelEvent{
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [_entryController release];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)saveEvent:(NSString *)albumName{
    //self.title = albumName;
    self.navigationController.navigationBar.userInteractionEnabled = YES;
    [_entryController.view removeFromSuperview];
    [_entryController release];
    NSLog(@"Album Name %@",albumName);
    if (myPhotos_ == nil) {
        myPhotos_ = [[Photos alloc] initWithFolderName:albumName];
        [myPhotos_ setDelegate:self];
    }
    [self setDataSource:myPhotos_];
}
#pragma mark -
#pragma mark PhotoPickerControllerDelegate

- (void)photoPickerController:(PhotoPickerController *)controller didFinishPickingWithImage:(UIImage *)image isFromCamera:(BOOL)isFromCamera
{
    [self showActivityIndicator];
    
    NSString * const key = @"nextNumber";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *nextNumber = [defaults valueForKey:key];
    if ( ! nextNumber ) {
        nextNumber = [NSNumber numberWithInt:1];
    }
    [defaults setObject:[NSNumber numberWithInt:([nextNumber intValue] + 1)] forKey:key];
    
    NSString *name = [NSString stringWithFormat:@"picture-%05i", [nextNumber intValue]];
    
    // Save to the photo album if picture is from the camera.
    [myPhotos_ savePhoto:image withName:name addToPhotoAlbum:isFromCamera];
}


#pragma mark -
#pragma mark PhotosDelegate

- (void)didFinishSave
{
    [self reloadThumbs];
}



@end
