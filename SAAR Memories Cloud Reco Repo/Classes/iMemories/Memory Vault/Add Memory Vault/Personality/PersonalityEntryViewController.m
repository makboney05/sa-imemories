//
//  PersonalityEntryViewController.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/9/13.
//
//

#import "PersonalityEntryViewController.h"

@interface PersonalityEntryViewController ()

- (void)handleViewLayout;
@end

@implementation PersonalityEntryViewController
@synthesize nameTxtField = _nameTxtField;
@synthesize tempermentTxtField = _tempermentTxtField;
@synthesize fearNAlergyTxtField = _fearNAlergyTxtField;
@synthesize lastConverTxtField = _lastConverTxtField;
@synthesize noteTxtView = _noteTxtView;
@synthesize profileImageView = _profileImageView;
@synthesize segmentControl = _segmentControl;
@synthesize tableView = _tableView;
@synthesize selectedFieldValue = _selectedFieldValue;
@synthesize selectedTemplate = _selectedTemplate;

static float displacement = 0.0f;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom
        if (!_expandedSections)
        {
            _expandedSections = [[NSMutableIndexSet alloc] init];
        }
    }
    return self;
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{

    [super touchesEnded:touches withEvent:event];
    [_currentTextField resignFirstResponder];
    
    [UIView animateWithDuration:.4 animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, 0);
    }];
}
- (void)configureToolBarForKeyboard{

    UIBarButtonItem *previousBttn = [[UIBarButtonItem alloc] initWithTitle:@"Previous" style:UIBarButtonItemStyleDone target:self action:@selector(prevBtnTapped)];
    
    
    UIBarButtonItem *nextBttn = [[UIBarButtonItem alloc] initWithTitle:@"Next" style:UIBarButtonItemStyleDone target:self action:@selector(nextBtnTapped)];
    
    
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *doneBttn = [[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneBtnTapped)];
    
    UIToolbar *keyBoardToolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    [keyBoardToolBar setItems:[NSArray arrayWithObjects:previousBttn,nextBttn,flexibleSpace,doneBttn, nil]];
    
    keyBoardToolBar.tintColor = [UIColor redColor];
    
    [_nameTxtField setInputAccessoryView:keyBoardToolBar];
    [_tempermentTxtField setInputAccessoryView:keyBoardToolBar];
    [_fearNAlergyTxtField setInputAccessoryView:keyBoardToolBar];
    [_lastConverTxtField setInputAccessoryView:keyBoardToolBar];
    [_relationTxtField setInputAccessoryView:keyBoardToolBar];
    [_noteTxtView setInputAccessoryView:keyBoardToolBar];
    
    [previousBttn release];
    [nextBttn release];
    [flexibleSpace release];
    [keyBoardToolBar release];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _appDelegate = (StartAppDelegate *)[UIApplication sharedApplication].delegate;
    
    self.tableView.dataSource =self;
    self.tableView.delegate = self;
    [self configureToolBarForKeyboard];
    if (_selectedFieldValue) {
        NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:_selectedFieldValue.value];
        _nameTxtField.text = [dic objectForKey:@"Name"];
        _tempermentTxtField.text = [dic objectForKey:@"Temperment"];
        _noteTxtView.text = [dic objectForKey:@"Note"];
        _fearNAlergyTxtField.text = [dic objectForKey:@"FearsAndAlergy"];
        _lastConverTxtField.text = [dic objectForKey:@"Last Conversation"];
        _relationTxtField.text = [dic objectForKey:@"Relation"];
        _profileImageView.image = [UIImage imageWithData:[dic objectForKey:@"profileImage"]];
        //self.title = _nameTxtField.text;
    }else{
        _nameTxtField.text = @"Personality";
    }
    
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnProfImage)];
    recognizer.numberOfTapsRequired = 1;
    [_profileImageView setUserInteractionEnabled:YES];
    [_profileImageView addGestureRecognizer:recognizer];
    [recognizer release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [_nameTxtField release];
    [_tempermentTxtField release];
    [_fearNAlergyTxtField release];
    [_lastConverTxtField release];
    [_noteTxtView release];
    [_profileImageView release];
    [_selectedTemplate release];
    [_selectedFieldValue release];
    [_photoPicker release];
}
#pragma mark -
#pragma mark - Button Event Handler

- (void)tappedOnProfImage{
    if (!_photoPicker) {
        _photoPicker = [[PhotoPickerController alloc] initWithDelegate:self];
    }
    [_photoPicker show];
}

- (IBAction)cancelButtonTapped:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)saveButtonTapped:(id)sender{
    if (!_selectedFieldValue) {
        
        NSData *imageData = UIImageJPEGRepresentation(_profileImageView.image, 0.8);
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setObject:_nameTxtField.text forKey:@"Name"];
        [dic setObject:_noteTxtView.text forKey:@"Note"];
        [dic setObject:_tempermentTxtField.text forKey:@"Temperment"];
        [dic setObject:_fearNAlergyTxtField.text forKey:@"FearsAndAlergy"];
        [dic setObject:_lastConverTxtField.text forKey:@"Last Conversation"];
        [dic setObject:_relationTxtField.text forKey:@"Relation"];
        [dic setObject:imageData forKey:@"profileImage"];
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
        
        FieldValue *fieldValue = (FieldValue *)[NSEntityDescription insertNewObjectForEntityForName:@"FieldValue" inManagedObjectContext:_appDelegate.managedObjectContext];
        [fieldValue setValue:data];
        [fieldValue setCreatedBy:[@"admin" uppercaseString]];
        [fieldValue setUpdatedBy:[@"admin" uppercaseString]];
        [fieldValue setCreatedDate:[NSDate date]];
        [fieldValue setUpdatedDate:[NSDate date]];
        [fieldValue setTags:[self getTagValue]];
        [fieldValue setType:[self getType]];
        [fieldValue setThetemplate:_selectedTemplate];
        
        [dic release];
        NSError *error = nil;
        if (![_appDelegate.managedObjectContext save:&error]) {
            //Handle Error
            
            // TO DO
            NSLog(@"Handle Error");
        }else{
            
            [self.navigationController popViewControllerAnimated:YES];
        }
    }else{
        // TO DO
        
    }
    
}

-(void)prevBtnTapped{

    if (_currentTextField.tag > PERSONTXTFIELD) {
        int tag = _currentTextField.tag;
        UITextField *prevNextField = (UITextField *)[self.view viewWithTag:--tag];
        [prevNextField becomeFirstResponder];
        [self handleViewLayout];
    }
}

-(void)nextBtnTapped{
    NSLog(@"current tag %d",_currentTextField.tag);
    if (_currentTextField.tag < NOTETXTFIELD) {
        int tag = _currentTextField.tag;
        UITextField *nextNextField = (UITextField *)[self.view viewWithTag:++tag];
        NSLog(@"next tag %d",nextNextField.tag);
        [nextNextField becomeFirstResponder];
        [self handleViewLayout];
        
    }
}

-(void)doneBtnTapped{
    [_currentTextField resignFirstResponder];
}


#pragma mark -
#pragma mark - PhotoPickerControllerDelegate

- (void)photoPickerController:(PhotoPickerController *)controller didFinishPickingWithImage:(UIImage *)image isFromCamera:(BOOL)isFromCamera
{
    //[self showActivityIndicator];
    
    _profileImageView.image = image;
    _profileImageView.contentMode = UIViewContentModeScaleAspectFit;
}

#pragma mark -
#pragma mark - Expanding

- (BOOL)tableView:(UITableView *)tableView canCollapseSection:(NSInteger)section
{
    if (section == 0) return YES;
    
    return NO;
}

#pragma mark -
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self tableView:tableView canCollapseSection:section])
    {
        if ([_expandedSections containsIndex:section])
        {
            NSLog(@"count %d",_appDelegate.listedEvents.count);
            return [_appDelegate.listedEvents count] + 1; // return rows when expanded
        }
        
        return 1; // only top row showing
    }
    
    // Return the number of rows in the section.
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    // Configure the cell...
    
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            // first row
            cell.textLabel.text = @"Register This Memory With Event"; // only top row showing
            UIFont *myFont = [ UIFont systemFontOfSize:17.0f];
            cell.textLabel.font  = myFont;
            if ([_expandedSections containsIndex:indexPath.section])
            {
                cell.textLabel.textColor = [UIColor grayColor];
                //cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeUp];
            }
            else
            {
                cell.textLabel.textColor = [UIColor lightGrayColor];
                //cell.accessoryView = [DTCustomColoredAccessory accessoryWithColor:[UIColor grayColor] type:DTCustomColoredAccessoryTypeDown];
            }
        }
        else
        {
            // all other rows
            RegisteredEvent *Revent = [_appDelegate.listedEvents objectAtIndex:indexPath.row - 1];
            cell.textLabel.text = Revent.title;
            NSLog(@"Event Name %d",indexPath.row);
            cell.textLabel.textColor = [UIColor blackColor];
            UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 15.0 ];
            cell.textLabel.font  = myFont;
            cell.accessoryView = nil;
            cell.textLabel.adjustsFontSizeToFitWidth = YES;
            
            NSArray *tagsArray = [_selectedFieldValue.tags componentsSeparatedByString:@","];
            if ([tagsArray containsObject:Revent.title]) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            }
            
        }
    }
    else
    {
        cell.accessoryView = nil;
        cell.textLabel.text = @"Normal Cell";
        
    }
    
    return cell;
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 34;
}

#pragma mark -
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [_nameTxtField resignFirstResponder];
    [_noteTxtView resignFirstResponder];
    if ([self tableView:tableView canCollapseSection:indexPath.section])
    {
        if (!indexPath.row)
        {
            [self.tableView beginUpdates];
            
            // only first row toggles exapand/collapse
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
            
            NSInteger section = indexPath.section;
            BOOL currentlyExpanded = [_expandedSections containsIndex:section];
            NSInteger rows;
            
            NSMutableArray *tmpArray = [NSMutableArray array];
            
            if (currentlyExpanded)
            {
                rows = [self tableView:tableView numberOfRowsInSection:section];
                [_expandedSections removeIndex:section];
                
            }
            else
            {
                [_expandedSections addIndex:section];
                rows = [self tableView:tableView numberOfRowsInSection:section];
            }
            
            for (int i=1; i<rows; i++)
            {
                NSIndexPath *tmpIndexPath = [NSIndexPath indexPathForRow:i inSection:section];
                [tmpArray addObject:tmpIndexPath];
            }
            
            if (currentlyExpanded)
            {
                [tableView deleteRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
                
            }
            else
            {
                [tableView insertRowsAtIndexPaths:tmpArray
                                 withRowAnimation:UITableViewRowAnimationTop];
            }
            
            [self.tableView endUpdates];
        }else{
            if (!_selectedIndexes) {
                _selectedIndexes = [[NSMutableArray alloc] init];
            }
            UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
            
            if ([selectedCell accessoryType] == UITableViewCellAccessoryNone) {
                [selectedCell setAccessoryType:UITableViewCellAccessoryCheckmark];
                [_selectedIndexes addObject:[NSNumber numberWithInt:indexPath.row]];
            } else {
                [selectedCell setAccessoryType:UITableViewCellAccessoryNone];
                [_selectedIndexes removeObject:[NSNumber numberWithInt:indexPath.row]];
            }
            
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            
        }
    }
}

#pragma mark -
#pragma mark - Class Methods
- (NSString *)getType{
    
    switch (_segmentControl.selectedSegmentIndex) {
        case 0:
            return @"PUBLIC";
            break;
            
        case 1:
            return @"PRIVATE";
            break;
            
        case 2:
            return @"SHARED";
            break;
            
        default: return @"PUBLIC";
            break;
    }
}
- (NSString *)getTagValue{
    NSString *tagValue = @"";//[[NSMutableString alloc] initWithCapacity:[_selectedIndexes count]];
    for (NSNumber *index in _selectedIndexes) {
        RegisteredEvent *evnt = [_appDelegate.listedEvents objectAtIndex:index.intValue];
        NSLog(@"event title %@",evnt.title);
        tagValue = [tagValue stringByAppendingFormat:@"%@,",evnt.title];
        NSLog(@"tagval %@",tagValue);
    }
    return tagValue;
}

- (void)handleViewLayout{

    if (_currentTextField.tag > _preveTextField.tag) {
        displacement -= 40.0f;
    }else displacement += 40.0f;
    [UIView animateWithDuration:.4 animations:^{
        self.view.transform = CGAffineTransformMakeTranslation(0, displacement);
    }];
    
}

#pragma mark -
#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{

    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{

    _currentTextField = textField;
    //[self handleViewLayout];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{

    _preveTextField = textField;
}
@end
