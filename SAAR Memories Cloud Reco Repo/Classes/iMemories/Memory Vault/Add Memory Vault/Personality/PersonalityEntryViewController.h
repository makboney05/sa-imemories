//
//  PersonalityEntryViewController.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/9/13.
//
//
#import "Template.h"
#import "FieldValue.h"
#import <UIKit/UIKit.h>
#import "StartAppDelegate.h"
#import "PhotoPickerController.h"

#define PERSONTXTFIELD 101
#define TEMPERTXTFIELD 102
#define FEARSTXTFIELD 103
#define LASTCONVOTXTFIELD 105
#define RELATIONTXTFIELD 101
#define NOTETXTFIELD 106

@interface PersonalityEntryViewController : UIViewController<UITextFieldDelegate,UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,PhotoPickerControllerDelegate>
{
    NSMutableIndexSet *_expandedSections;
    StartAppDelegate *_appDelegate;
    NSMutableArray *_selectedIndexes;
    PhotoPickerController *_photoPicker;
    
    UITextField *_currentTextField;
    UITextField *_preveTextField;
}

@property (nonatomic, strong) IBOutlet UITextField *nameTxtField;
@property (nonatomic, strong) IBOutlet UITextField *tempermentTxtField;
@property (nonatomic, strong) IBOutlet UITextField *fearNAlergyTxtField;
@property (nonatomic, strong) IBOutlet UITextField *lastConverTxtField;
@property (nonatomic, strong) IBOutlet UITextField *relationTxtField;

@property (nonatomic, strong) IBOutlet UITextView *noteTxtView;

@property (nonatomic, strong) IBOutlet UIImageView *profileImageView;

@property (nonatomic, strong) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IBOutlet UISegmentedControl *segmentControl;

@property (nonatomic, retain) Template *selectedTemplate;
@property (nonatomic, retain) FieldValue *selectedFieldValue;
@end
