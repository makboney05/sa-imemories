//
//  MemoryVaultViewController.m
//  iMemories
//
//  Created by Lion Boney on 2/14/13.
//  Copyright (c) 2013 surroundapps. All rights reserved.
//

#import "MemoryVaultViewController.h"

#import "AddMemoryVaultController.h"

#import "UIBarButtonItem+WEPopover.h"

#import "CategoryViewController.h"


@interface MemoryVaultViewController ()

@end

@implementation MemoryVaultViewController
@synthesize cameraViewController = _cameraViewController;
@synthesize popoverController;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize memoryVaultTblView = _memoryVaultTblView;
@synthesize templateType = _templateType;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Memory Vault", nil);
        self.tabBarItem.image = [UIImage imageNamed:@"notepad"];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self.navigationController setNavigationBarHidden:NO];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addItems:)];
    self.navigationItem.rightBarButtonItem = addButton;
    [addButton release];
    
    /*UIBarButtonItem *scanButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCamera target:self action:@selector(scanItems:)];
    self.navigationItem.leftBarButtonItem = scanButton;
    [scanButton release];*/
    
    popoverClass = [WEPopoverController class];
    
    //[self saveTemplate];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Template" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    
    NSError *error = nil;
    NSMutableArray *mutableFetchresults = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    for (Template *t in mutableFetchresults) {
        NSLog(@"%d",[t.field count]);
    }
    
    _cameraViewController = [[CRParentViewController alloc] initWithNibName:nil bundle:nil];
    
    // need to set this so subsequent view controllers know the size
    _cameraViewController.arViewSize = self.view.bounds.size;
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@""] forBarMetrics:UIBarMetricsDefault];
}

-(void) viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    SCNavigationBar *navBar = (SCNavigationBar *)self.navigationController.navigationBar;
    [navBar setTintColor:[UIColor colorWithWhite:0.878 alpha:1.000]];
    if ([_templateType isEqualToString:@"Album"]) {
        [navBar setBackgroundImage:[UIImage imageNamed:@"album_nav_bar"] forBarMetrics:UIBarMetricsDefault];
        
    }else if ([_templateType isEqualToString:@"Event"]) {
    
        [navBar setBackgroundImage:[UIImage imageNamed:@"event_nav_bar"] forBarMetrics:UIBarMetricsDefault];
        
    }else if ([_templateType isEqualToString:@"Location"]) {
        
        [navBar setBackgroundImage:[UIImage imageNamed:@"loc_nav_bar"] forBarMetrics:UIBarMetricsDefault];
    }else{
    
        [navBar setBackgroundImage:[UIImage imageNamed:@"personality_nav_bar"] forBarMetrics:UIBarMetricsDefault];
        
    }
    self.title = @"";
    
    NSString *predicateString = [NSString stringWithFormat:@"thetemplate.templateType.name==\"%@\"", _templateType];
    NSPredicate *requestPredicate = [NSPredicate predicateWithFormat:predicateString];
    
    self.fetchedResultsController.fetchRequest.predicate = requestPredicate;
    NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    _memoryVaultTblView = nil;
}

#pragma mark -
#pragma mark - Event Handler
/*- (void)scanItems:(UIBarButtonItem *)scanButton{
    [self.navigationController pushViewController:_cameraViewController animated:YES];
}*/
- (void)addItems:(UIBarButtonItem *)addButton{
    
    /*if (!self.popoverController) {
		
		contentViewController = [[CategoryViewController alloc] initWithNibName:@"CategoryViewController" bundle:nil];
        contentViewController.passValueDelegate = self;
		self.popoverController = [[[popoverClass alloc] initWithContentViewController:contentViewController] autorelease];
		self.popoverController.delegate = self;
		self.popoverController.passthroughViews = [NSArray arrayWithObject:self.navigationController.navigationBar];
		contentViewController.managedObjectContext = _managedObjectContext;
		[self.popoverController presentPopoverFromBarButtonItem:addButton
									   permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown)
													   animated:YES];
        
		[contentViewController release];
	} else {
		[self.popoverController dismissPopoverAnimated:YES];
		self.popoverController = nil;
	}*/

    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Template" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"(templateType.name = %@)", _templateType];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    Template *templateModel = (Template*) [[self.managedObjectContext executeFetchRequest:request error:&error] objectAtIndex:0];
    
    if ([_templateType isEqualToString:@"Album"] || [_templateType isEqualToString:@"Location"] ||[_templateType isEqualToString:@"Event"]) {
        
        AddAlbumViewController *addcontroller = [[AddAlbumViewController alloc]initWithNibName:@"AddAlbumViewController" bundle:nil];
        addcontroller.selectedTemplate = templateModel;
        addcontroller.selectedFieldValue = nil;
        self.title = @"Back";
        [self.navigationController pushViewController:addcontroller animated:YES];
        [addcontroller release];
    }else{
        //PersonalityEntryViewController
        
        PersonalityEntryViewController *addPersonalitycontroller = nil;
        
        if (IS_IPHONE_5) addPersonalitycontroller = [[PersonalityEntryViewController alloc]initWithNibName:@"PersonalityEntryViewController" bundle:nil];
        else addPersonalitycontroller = [[PersonalityEntryViewController alloc]initWithNibName:@"PersonalityEntryViewController-iPhonePre" bundle:nil];
        addPersonalitycontroller.selectedTemplate = templateModel;
        addPersonalitycontroller.selectedFieldValue = nil;
        self.title = @"Back";
        [self.navigationController pushViewController:addPersonalitycontroller animated:YES];
        [addPersonalitycontroller release];
    }
}

#pragma mark -
#pragma mark - Class Methods
- (void)reloadView{

    NSLog(@"Reloaded");
}
#pragma mark -
#pragma mark - WEPopoverControllerDelegate implementation

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)thePopoverController {
	//Safe to release the popover here
	self.popoverController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)thePopoverController {
	//The popover is automatically dismissed if you click outside it, unless you return NO here
	return YES;
}
#pragma mark -
#pragma mark - PassTypeTomemoryVaultEntry Delegate implementation
-(void)valueHasChosen:(id)selectedValue{
    WEPopoverController *controller = (WEPopoverController*)contentViewController;
    [self popoverControllerDidDismissPopover:controller];
    
    TemplateType *model = selectedValue;
    NSLog(@"%@ type",model.name);
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Template" inManagedObjectContext:_managedObjectContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat: @"(templateType = %@)", model];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    [request setPredicate:predicate];
    
    NSError *error = nil;
    Template *templateModel = (Template*) [[self.managedObjectContext executeFetchRequest:request error:&error] objectAtIndex:0];
    if ([model.name isEqualToString:@"Album"] || [model.name isEqualToString:@"Location"] ||[model.name isEqualToString:@"Event"]) {
        AddAlbumViewController *addcontroller = [[AddAlbumViewController alloc]initWithNibName:@"AddAlbumViewController" bundle:nil];
        addcontroller.selectedTemplate = templateModel;
        addcontroller.selectedFieldValue = nil;
        self.title = @"Back";
        [self.navigationController pushViewController:addcontroller animated:YES];
        [addcontroller release];
    }else{
    //PersonalityEntryViewController
        
        PersonalityEntryViewController *addPersonalitycontroller = nil;
        
        if (IS_IPHONE_5) addPersonalitycontroller = [[PersonalityEntryViewController alloc]initWithNibName:@"PersonalityEntryViewController" bundle:nil];
        else addPersonalitycontroller = [[PersonalityEntryViewController alloc]initWithNibName:@"PersonalityEntryViewController-iPhonePre" bundle:nil];
        addPersonalitycontroller.selectedTemplate = templateModel;
        addPersonalitycontroller.selectedFieldValue = nil;
        self.title = @"Back";
        [self.navigationController pushViewController:addPersonalitycontroller animated:YES];
        [addPersonalitycontroller release];
    }
}

#pragma mark - 
#pragma mark temp
- (void)saveTemplate{
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Template" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entity];
    
    NSError *error = nil;
    NSMutableArray *mutableFetchresults = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchresults == nil) {
        //Handle Error
        NSLog(@"Handle Fetch error");
    }
    if ([mutableFetchresults count] >0) {
        return;
    }
    
    NSEntityDescription *entityForFields = [NSEntityDescription entityForName:@"Field" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entityForFields];
    
    NSMutableArray *mutableFetchresultsForFields = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchresultsForFields == nil) {
        //Handle Error
        NSLog(@"Handle Fetch error");
    }
    
    NSEntityDescription *entityForTempletTypes = [NSEntityDescription entityForName:@"TemplateType" inManagedObjectContext:self.managedObjectContext];
    [request setEntity:entityForTempletTypes];
    
    NSMutableArray *mutableFetchresultsForTemplateTypes = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchresultsForTemplateTypes == nil) {
        //Handle Error
        NSLog(@"Handle Fetch error");
    }
    TemplateType *albumTempType = nil, *passwordTempType = nil, *accountTempType = nil;
    for (TemplateType *tempTemType in mutableFetchresultsForTemplateTypes) {
        if ([tempTemType.name isEqualToString:@"Album"]) {
            albumTempType = tempTemType;
        }
        if ([tempTemType.name isEqualToString:@"Bank Account"]) {
            accountTempType = tempTemType;
        }
        if ([tempTemType.name isEqualToString:@"Password"]) {
            passwordTempType = tempTemType;
        }
    }
    NSMutableArray *fields4EntryAccount = [[NSMutableArray alloc] init];
    NSMutableArray *fields4EntryPassword = [[NSMutableArray alloc] init];
    NSMutableArray *fields4EntryAlbum = [[NSMutableArray alloc] init];
    for (Field *tField in mutableFetchresultsForFields) {
        
        if ([tField.name isEqualToString:@"User Name"]) {
            [fields4EntryPassword addObject:tField];
        }else if ([tField.name isEqualToString:@"Website Address"]) {
            [fields4EntryPassword addObject:tField];
        }else if ([tField.name isEqualToString:@"Password"]) {
            [fields4EntryPassword addObject:tField];
        }else if ([tField.name isEqualToString:@"Pin Code"]) {
            [fields4EntryPassword addObject:tField];
        }else if ([tField.name isEqualToString:@"Note"]) {
            [fields4EntryPassword addObject:tField];
            [fields4EntryAccount addObject:tField];
            [fields4EntryAlbum addObject:tField];
        }else if ([tField.name isEqualToString:@"Title"]) {
            [fields4EntryPassword addObject:tField];
            [fields4EntryAccount addObject:tField];
            [fields4EntryAlbum addObject:tField];
        }else if ([tField.name isEqualToString:@"Attachment"]) {
            [fields4EntryAccount addObject:tField];
            [fields4EntryPassword addObject:tField];
        }else if ([tField.name isEqualToString:@"Account Number"]) {
            [fields4EntryAccount addObject:tField];
        }else if ([tField.name isEqualToString:@"Account Type"]) {
            [fields4EntryAccount addObject:tField];
        }else if ([tField.name isEqualToString:@"Bank Name"]) {
            [fields4EntryAccount addObject:tField];
        }else if ([tField.name isEqualToString:@"Routing Number"]) {
            [fields4EntryAccount addObject:tField];
        }else if ([tField.name isEqualToString:@"Album"]) {
            [fields4EntryAlbum addObject:tField];
        }
    }
    NSLog(@"count %d",[fields4EntryPassword count]);
    Template *templateSAPassWord = (Template *)[NSEntityDescription insertNewObjectForEntityForName:@"Template" inManagedObjectContext:self.managedObjectContext];
    [templateSAPassWord setName:@"com.surroundapps.password"];
    [templateSAPassWord setCreatedBy:[@"admin" uppercaseString]];
    [templateSAPassWord setUpdatedBy:[@"admin" uppercaseString]];
    [templateSAPassWord setCreatedDate:[NSDate date]];
    [templateSAPassWord setUpdatedDate:[NSDate date]];
    NSSet *setttt = [[NSSet alloc] initWithArray:fields4EntryPassword];
    NSLog(@"count %d",[setttt count]);
    [templateSAPassWord setField:setttt];
    [setttt release];
    [templateSAPassWord setTemplateType:accountTempType];
    [fields4EntryPassword release];
    
    
    Template *templateSABankAccount = (Template *)[NSEntityDescription insertNewObjectForEntityForName:@"Template" inManagedObjectContext:self.managedObjectContext];
    [templateSABankAccount setName:@"com.surroundapps.bankaccount"];
    [templateSABankAccount setCreatedBy:[@"admin" uppercaseString]];
    [templateSABankAccount setUpdatedBy:[@"admin" uppercaseString]];
    [templateSABankAccount setCreatedDate:[NSDate date]];
    [templateSABankAccount setUpdatedDate:[NSDate date]];
    [templateSABankAccount setField:[[NSSet alloc] initWithArray:fields4EntryAccount]];
    [templateSABankAccount setTemplateType:accountTempType];
    
    
    
    Template *templateSAAlbum = (Template *)[NSEntityDescription insertNewObjectForEntityForName:@"Template" inManagedObjectContext:self.managedObjectContext];
    [templateSAAlbum setName:@"com.surroundapps.album"];
    [templateSAAlbum setCreatedBy:[@"admin" uppercaseString]];
    [templateSAAlbum setUpdatedBy:[@"admin" uppercaseString]];
    [templateSAAlbum setCreatedDate:[NSDate date]];
    [templateSAAlbum setUpdatedDate:[NSDate date]];
    NSSet *sett = [[NSSet alloc] initWithArray:fields4EntryAlbum];
    [templateSAAlbum setField:sett];
    [sett release];
    [templateSAAlbum setTemplateType:albumTempType];
    
    
    if (![self.managedObjectContext save:&error]) {
        //Handle Error
        NSLog(@"Handle Error");
    }else{
        
        //[self.navigationController popViewControllerAnimated:YES];
    }
    
}

#pragma mark -
#pragma mark - TableView Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    
    id  sectionInfo =
    [[_fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    FieldValue *info = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:info.value];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"dd,MMMM YY"];

    cell.textLabel.text = [dic objectForKey:@"Name"];
        cell.detailTextLabel.text = [format stringFromDate:info.createdDate];
    [cell.detailTextLabel setTextColor:[UIColor lightGrayColor]];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        FieldValue *fieldValToDelete = [_fetchedResultsController objectAtIndexPath:indexPath];
        
        _fieldValToDelete = fieldValToDelete;
        _alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:[NSString stringWithFormat:@"Do you want to delete this item?"] delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        [_alertView show];
        [_alertView release];
        
    }
    //    else if (editingStyle == UITableViewCellEditingStyleInsert) {
    //        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    //    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{    
    
    return 47.0f;
}
#pragma mark -
#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
     */
    
    FieldValue *info = [_fetchedResultsController objectAtIndexPath:indexPath];
    NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:info.value];
    NSLog(@"name %@",info.thetemplate.templateType.name);
    if ([info.thetemplate.templateType.name isEqualToString:@"Album"] || [info.thetemplate.templateType.name isEqualToString:@"Location"] || [info.thetemplate.templateType.name isEqualToString:@"Event"]) {
        AddAlbumViewController *addcontroller = [[AddAlbumViewController alloc]initWithNibName:@"AddAlbumViewController" bundle:nil];
        addcontroller.selectedTemplate = nil;
        addcontroller.selectedFieldValue = info;
        addcontroller.albumName = [dic objectForKey:@"Name"];
        self.title = @"Back";
        [self.navigationController pushViewController:addcontroller animated:YES];
        [addcontroller release];
    }else{
    
        PersonalityEntryViewController *addcontroller = [[PersonalityEntryViewController alloc]initWithNibName:@"PersonalityEntryViewController" bundle:nil];
        addcontroller.selectedTemplate = nil;
        addcontroller.selectedFieldValue = info;
        self.title = @"Back";
        [self.navigationController pushViewController:addcontroller animated:YES];
        [addcontroller release];
    }
    
}
#pragma mark -
#pragma mark - fetchedResultsController

- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"FieldValue" inManagedObjectContext:_managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sort = [[NSSortDescriptor alloc]
                              initWithKey:@"createdDate" ascending:NO];
    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sort]];
    
    [fetchRequest setFetchBatchSize:20];
    
    [sort release];
    
    
    
    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:_managedObjectContext sectionNameKeyPath:nil
                                                   cacheName:nil];
    
    self.fetchedResultsController = theFetchedResultsController;
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
    
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.memoryVaultTblView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.memoryVaultTblView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.memoryVaultTblView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.memoryVaultTblView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.memoryVaultTblView endUpdates];
}

#pragma mark - UIAlertView Delegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if (buttonIndex == 1) {
        BOOL albmDeleted;
        NSLog(@"%@",_fieldValToDelete.thetemplate.templateType.name);
        if(![_fieldValToDelete.thetemplate.templateType.name isEqualToString:@"Personality"]){
            NSDictionary *dic = [NSKeyedUnarchiver unarchiveObjectWithData:_fieldValToDelete.value];
            NSString *albmName = [dic objectForKey:@"Name"];
            Photos *photos = [[Photos alloc]initWithFolderName:albmName];
            NSLog(@"Album Name %@",albmName);
            if ([photos deleteAlbum]) {
                albmDeleted = YES;
            }else{
                albmDeleted = NO;
            }
            [photos release];
        }else albmDeleted = YES;
        if (albmDeleted) {
            [_managedObjectContext deleteObject:_fieldValToDelete];
            
            NSError *error = nil;
            if (![_managedObjectContext save:&error]) {
                //Handle Error
                NSLog(@"Handle Delete Error");
                _alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Couln't delete the item, please try later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
                [_alertView show];
                [_alertView release];
            }
        }else{
        
        
            _alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Couln't delete the item, please try later." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
            [_alertView show];
            [_alertView release];
        }
        
        
    }
}
@end
