//
//  RegisterTargetViewController.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/10/13.
//
//


#import "Base64.h"
#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "MBProgressHUD.h"
#import "StartAppDelegate.h"
#import "PhotoPickerController.h"


@interface RegisterTargetViewController : UIViewController<PhotoPickerControllerDelegate,UITextFieldDelegate,NSURLConnectionDelegate,UITableViewDataSource,UITableViewDelegate,MBProgressHUDDelegate,UIAlertViewDelegate>
{
    PhotoPickerController *_photoPicker;
    StartAppDelegate *_appDelegate;
    
    NSString *_metaData;
    UIImage *_selectedImage;
    
    UIAlertView *_alertView;
    
    MBProgressHUD *_hud;
}

@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UITextField *nameTxtField;

@property (nonatomic, retain) IBOutlet UITableView *occassionTableView;

@property (nonatomic, retain) NSMutableData *receivedData;

- (IBAction)registerBttnTapped:(id)sender;
- (IBAction)cancelBttnTapped:(id)sender;
@end
