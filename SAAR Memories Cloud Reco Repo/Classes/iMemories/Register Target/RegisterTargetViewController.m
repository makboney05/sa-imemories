//
//  RegisterTargetViewController.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/10/13.
//
//

#import "RegisterTargetViewController.h"

@interface RegisterTargetViewController ()

@end

@implementation RegisterTargetViewController
@synthesize imageView = _imageView;
@synthesize nameTxtField = _nameTxtField;
@synthesize receivedData = _receivedData;
@synthesize occassionTableView = _occassionTableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{

    [super touchesEnded:touches withEvent:event];
    [_nameTxtField resignFirstResponder];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    _selectedImage = nil;
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnProfImage)];
    recognizer.numberOfTapsRequired = 1;
    [_imageView setUserInteractionEnabled:YES];
    [_imageView addGestureRecognizer:recognizer];
    [recognizer release];
    
    _appDelegate = (StartAppDelegate *)[UIApplication sharedApplication].delegate;
    
    UIBarButtonItem *logonBttn = [[UIBarButtonItem alloc] initWithTitle:@"Register" style:UIBarButtonItemStyleDone target:self action:@selector(registerBttnTapped:)];
    self.navigationItem.rightBarButtonItem = logonBttn;
    [logonBttn release];
    
    UIBarButtonItem *cancelBttn = [[UIBarButtonItem alloc] initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelBttnTapped:)];
    self.navigationItem.leftBarButtonItem = cancelBttn;
    [cancelBttn release];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"nav_bar_reg"] forBarMetrics:UIBarMetricsDefault];
    
    _hud = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:_hud];
    _hud.userInteractionEnabled = NO;
    _hud.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [_imageView release];
    [_nameTxtField release];
}

#pragma mark -
#pragma mark - Button Hadler

- (void)tappedOnProfImage{
    if (!_photoPicker) {
        _photoPicker = [[PhotoPickerController alloc] initWithDelegate:self];
    }
    [_photoPicker show];
    
}

- (IBAction)cancelBttnTapped:(id)sender{

    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)registerBttnTapped:(id)sender{

    if ([_nameTxtField.text length] == 0) {
        _alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Please enter a valid name." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [_alertView show];
        [_alertView release];
        return;
    }
    if ([_metaData length] == 0) {
        _alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Please select a relevant event from list." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [_alertView show];
        [_alertView release];
        return;
    }
    if (!_selectedImage) {
        _alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Please chose an photo." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [_alertView show];
        [_alertView release];
        return;
    }
    [_hud show:YES];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if (internetStatus != NotReachable) {
        
        [Base64 initialize];
        NSData *imageData1 = UIImagePNGRepresentation(_selectedImage);
        NSLog(@"before %u",[imageData1 length]);
        NSData *imageData = UIImageJPEGRepresentation(_selectedImage, .8);
        CGFloat compression = 0.9f;
        CGFloat maxCompression = 0.1f;
        int maxFileSize = 200*1024;
        
        while ([imageData length] > maxFileSize && compression > maxCompression)
        {
            compression -= 0.1;
            imageData = UIImageJPEGRepresentation(_selectedImage, compression);
        }
        
        _imageView.image = [UIImage imageWithData:imageData];
        NSLog(@"after %u",[imageData length]);
        NSString *imageString = [Base64 encode:imageData];
        NSLog(@"__metaData %@",_metaData);
        NSArray *objects = [NSArray arrayWithObjects:_nameTxtField.text,imageString,_metaData,nil];
        NSArray *keys = [NSArray arrayWithObjects:@"targetName", @"targetImage", @"metaData", nil];
        NSDictionary *targetDic = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:targetDic
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        NSString *jsonRequest;
        if (! jsonData) {
            NSLog(@"Got an error: %@", error);
        } else {
            jsonRequest = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        }
        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://192.168.102.5:8000/VuforiaService.svc/AddTarget"]
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:60.0];
        
        
        [request setHTTPMethod:@"POST"];
        
        [request setValue:@"plain/text" forHTTPHeaderField:@"Content-Type"];
        [request setValue:[NSString stringWithFormat:@"%d", [jsonData length]] forHTTPHeaderField:@"Content-Length"];
        [request setHTTPBody: jsonData];
        
        NSURLConnection *connection = [[NSURLConnection alloc]initWithRequest:request delegate:self];
        if (connection) {
            _receivedData = [[NSMutableData data] retain];
        }
        [_metaData release];
    }
    else {
        [_hud hide:YES];
        _alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Internet is not reachable at this time, please try later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [_alertView show];
        [_alertView release];
    }
    
}

#pragma mark -
#pragma mark - Connection Handler
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{

    NSLog(@"Error %@",error);
    [_hud hide:YES];
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_receivedData appendData:data];
    [_hud hide:YES];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [_hud hide:YES];
    NSError *error;
    NSString *dataString = [[[NSString alloc] initWithData:_receivedData
                                                  encoding:NSUTF8StringEncoding] autorelease];
    NSLog(@"dataString %@",dataString);
    NSDictionary *rootDictionary = [NSJSONSerialization JSONObjectWithData:_receivedData options:NSJSONReadingMutableContainers error:&error];
    NSDictionary *addTargetResultDic = [rootDictionary objectForKey:@"addTargetResultDic"];
    /*if (!addTargetResultDic) {
        _alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Failed to register photo. Please try later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:, nil];
        [_alertView show];
        [_alertView release];
        return;
    }*/
    _alertView = [[UIAlertView alloc] initWithTitle:@"Info" message:@"Photo successfully registered. Do you want to register another photo." delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    [_alertView show];
    [_alertView release];
}

#pragma mark - 
#pragma mark - UITextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{

    [_nameTxtField resignFirstResponder];
    return YES;
}

#pragma mark -
#pragma mark - PhotoPickerControllerDelegate

- (void)photoPickerController:(PhotoPickerController *)controller didFinishPickingWithImage:(UIImage *)image isFromCamera:(BOOL)isFromCamera
{
    //[self showActivityIndicator];
    
    _imageView.image = image;
    _imageView.contentMode = UIViewContentModeScaleAspectFit;
    _selectedImage = image;
}

#pragma mark -
#pragma mark - TableView Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    RegisteredEvent *Revent = [_appDelegate.listedEvents objectAtIndex:section];
    return [Revent.occasionArray count];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return [_appDelegate.listedEvents count];
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    RegisteredEvent *Revent = [_appDelegate.listedEvents objectAtIndex:indexPath.section];
    cell.textLabel.text = [Revent.occasionArray objectAtIndex:indexPath.row];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    // Configure the cell...
    //[self configureCell:cell atIndexPath:indexPath];
    RegisteredEvent *Revent = [_appDelegate.listedEvents objectAtIndex:indexPath.section];
    cell.textLabel.text = [Revent.occasionArray objectAtIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleGray;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    RegisteredEvent *Revent = [_appDelegate.listedEvents objectAtIndex:section];
    return Revent.title;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 34.0f;
}
#pragma mark -
#pragma mark - TableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RegisteredEvent *Revent = [_appDelegate.listedEvents objectAtIndex:indexPath.section];
    _metaData = [[[[Revent.occasionArray objectAtIndex:indexPath.row] stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString] stringByAppendingString:@".json"];
    NSLog(@"name %@",[_metaData retain]);
}

#pragma mark -
#pragma mark - UIAlertview Delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{

    if (buttonIndex == 0) {
        [self.navigationController popViewControllerAnimated:YES];
    }else{
    
        _nameTxtField.text = @"";
        _selectedImage = nil;
        _imageView.image = nil;
        [_occassionTableView reloadData];
        
    }
}
@end
