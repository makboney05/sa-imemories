//
//  EntryListViewController.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/12/13.
//
//

#import <UIKit/UIKit.h>

@interface EntryListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{

    
}
@property (nonatomic, retain) IBOutlet UITableView *eventTableView;
@property (nonatomic, retain) IBOutlet UITableView *occasrionTableView;

@end
