//
//  EntryListViewController.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/12/13.
//
//

#import "EntryListViewController.h"

@interface EntryListViewController ()

@end

@implementation EntryListViewController
@synthesize eventTableView = _eventTableView;
@synthesize occasrionTableView = _occasrionTableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
