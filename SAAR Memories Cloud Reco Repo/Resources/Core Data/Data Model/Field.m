//
//  Field.m
//  SAAR Memories
//
//  Created by Boney's Macmini on 5/12/13.
//
//

#import "Field.h"
#import "Template.h"


@implementation Field

@dynamic createdBy;
@dynamic createdDate;
@dynamic name;
@dynamic order;
@dynamic type;
@dynamic updatedBy;
@dynamic updatedDate;
@dynamic templatee;

@end
