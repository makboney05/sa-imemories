//
//  FieldValue.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/3/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Template;

@interface FieldValue : NSManagedObject

@property (nonatomic, retain) NSString * createdBy;
@property (nonatomic, retain) NSDate * createdDate;
@property (nonatomic, retain) NSString * tags;
@property (nonatomic, retain) NSString * updatedBy;
@property (nonatomic, retain) NSDate * updatedDate;
@property (nonatomic, retain) NSData * value;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) Template *thetemplate;

@end
