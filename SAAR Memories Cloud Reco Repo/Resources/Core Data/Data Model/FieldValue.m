//
//  FieldValue.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 6/3/13.
//
//

#import "FieldValue.h"
#import "Template.h"


@implementation FieldValue

@dynamic createdBy;
@dynamic createdDate;
@dynamic tags;
@dynamic updatedBy;
@dynamic updatedDate;
@dynamic value;
@dynamic type;
@dynamic thetemplate;

@end
