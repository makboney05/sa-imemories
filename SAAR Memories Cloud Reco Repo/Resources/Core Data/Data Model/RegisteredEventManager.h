//
//  RegisteredEventManager.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/28/13.
//
//

#import <Foundation/Foundation.h>
#import "RegisteredEventManagerDelegateProtocol.h"
@interface RegisteredEventManager : NSObject
{

    NSMutableData *regEventInfo;
    id <RegisteredEventManagerDelegateProtocol> delegate;
}
@property (readwrite, nonatomic, setter = cancelNetworkOperations:) BOOL cancelNetworkOperation;
@property (readonly, nonatomic, getter = isNetworkOperationInProgress) BOOL networkOperationInProgress;

+(RegisteredEventManager *)sharedInstance;

-(void)eventWithJSONFilename:(NSString *)jsonFilename withDelegate:(id <RegisteredEventManagerDelegateProtocol>)aDelegate;
@end
