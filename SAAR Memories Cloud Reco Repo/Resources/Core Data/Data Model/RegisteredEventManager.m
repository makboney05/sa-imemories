//
//  RegisteredEventManager.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/28/13.
//
//

#import "RegisteredEventManager.h"
#import "ImagesManager.h"
#import "EventsManagerDelegateProtocol.h"
@implementation RegisteredEventManager
@synthesize cancelNetworkOperation, networkOperationInProgress;

#define REGISTEREDEVENTSJSONURL @"https://copy.com/m9ZvsOXIFuCX/test"
static RegisteredEventManager *sharedInstance = nil;

#pragma mark - Public

- (void) eventWithJSONFilename:(NSString *)jsonFilename withDelegate:(id<RegisteredEventManagerDelegateProtocol>)aDelegate{


    networkOperationInProgress = YES;
    
    //  Get URL
    NSString *anURLString = [NSString stringWithFormat:@"%@/%@", REGISTEREDEVENTSJSONURL, jsonFilename];
    NSURL *anURL = [NSURL URLWithString:anURLString];
    NSLog(@"anURL %@",anURLString);

    delegate = aDelegate;
    [delegate retain];

    // Download the event info
    [self asyncDownloadInfoForEventAtURL:anURL];

}

-(void)asyncDownloadInfoForEventAtURL:(NSURL *)url
{
    // Download the info for this event
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] initWithURL:url] autorelease];
    [request setHTTPMethod:@"GET"];
    
    // Do not start the network operation immediately
    NSURLConnection *aConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO];
    
    // Use the run loop associated with the main thread
    [aConnection scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSDefaultRunLoopMode];
    
    // Start the network operation
    [aConnection start];
}

-(id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

+(RegisteredEventManager *)sharedInstance
{
	@synchronized(self)
    {
		if (sharedInstance == nil)
        {
			sharedInstance = [[self alloc] init];
		}
	}
	return sharedInstance;
}
-(BOOL)isNetworkOperationInProgress
{
    // The eventsManager or ImagesManager may have a network operation in
    // progress
    return networkOperationInProgress | [[ImagesManager sharedInstance] networkOperationInProgress] ? YES : NO;
}

-(void)cancelNetworkOperations:(BOOL)cancel
{
    // Set or clear the cancel flags, which will be checked in each network
    // callback
    
    // eventsManager (self)
    cancelNetworkOperation = cancel;
    
    // ImagesManager
    [[ImagesManager sharedInstance] setCancelNetworkOperation:cancel];
}
-(void)infoDownloadDidFinishWithEventData:(NSData *)eventData withConnection:(NSURLConnection *)connection
{
    RegisteredEvent *Revent = nil;
    NSMutableArray *eventList = nil;
    if (eventData)
    {
        //  Given a NSData, parse the event to a dictionary and then convert it into a event object
        NSError *anError = nil;
        NSDictionary *eventDictionary = nil;
        
        //  Find out on runtime if the device can use NSJSONSerialization (iOS5 or later)
        NSString *className = @"NSJSONSerialization";
        Class class = NSClassFromString(className);
        
        if (!class)
        {
            //  Use custom eventDataParser.
            //
            //  IMPORTANT: eventDataParser is written to parse data specific to the CloudReco
            //  sample application and is not designed to be used in other applications.
            
            //eventDictionary = [RegisteredEvent parseData:eventData];
            NSLog(@"#DEBUG Using custom JSONeventParser");
        }
        else
        {
            //  Use native JSON parser, NSJSONSerialization
            eventDictionary = [NSJSONSerialization JSONObjectWithData:eventData
                                                              options: NSJSONReadingMutableContainers
                                                                error: &anError];
            NSLog(@"#DEBUG Using NSJSONSerialization");
            NSArray *eventArray = [eventDictionary objectForKey:@"events"];
            eventList = [[NSMutableArray alloc] initWithCapacity:[eventArray count]];
            for (NSDictionary *dic in eventArray) {
                if (!dic)
                {
                    NSLog(@"#DEBUG Error parsing JSON: %@", anError);
                }
                else
                {
                    Revent = [[[RegisteredEvent alloc] initWithDictionary:dic] autorelease];
                    [eventList addObject: Revent];
                }
            }
        }
        
    }
    
    //  Inform the delegate that the request has completed
    [delegate infoRequestDidFinishForEvent:[eventList autorelease] byCancelling:[self cancelNetworkOperation]];
    
    if (YES == [self cancelNetworkOperation])
    {
        // Inform the ImagesManager that the network operation has already been
        // cancelled (so its network operation will not be started and therefore
        // does not need to be cancelled)
        [self cancelNetworkOperations:NO];
    }
    
    
    [delegate release];
    delegate = nil;
    
    [regEventInfo release];
    regEventInfo = nil;
    
    //  We don't need this connection reference anymore
    [connection release];
    
    networkOperationInProgress = NO;
}

#pragma mark NSURLConnectionDelegate
// *** These delegate methods are always called on the main thread ***
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self infoDownloadDidFinishWithEventData:nil withConnection:connection];
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSData *eventData = nil;
    
    if (YES == [self cancelNetworkOperation])
    {
        // Cancel this connection
        [connection cancel];
    }
    else if (regEventInfo)
    {
        eventData = [NSData dataWithData:regEventInfo];
    }
    
    [self infoDownloadDidFinishWithEventData:eventData withConnection:connection];
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (YES == [self cancelNetworkOperation])
    {
        // Cancel this connection
        [connection cancel];
        
        [self infoDownloadDidFinishWithEventData:nil withConnection:connection];
    }
    else
    {
        if (nil == regEventInfo)
        {
            regEventInfo = [[NSMutableData alloc] init];
        }
        
        [regEventInfo appendData:data];
    }
}


#pragma mark Singleton overrides

+ (id)allocWithZone:(NSZone *)zone
{
    //  Overriding this method for singleton
    
	@synchronized(self)
    {
		if (sharedInstance == nil)
        {
			sharedInstance = [super allocWithZone:zone];
			return sharedInstance;
		}
	}
	return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    //  Overriding this method for singleton
	return self;
}

- (id)retain
{
    //  Overriding this method for singleton
    return self;
}

- (NSUInteger)retainCount
{
    //  Overriding this method for singleton
	return NSUIntegerMax;
}

- (oneway void)release
{
    //  Overriding this method for singleton
}

- (id)autorelease
{
    //  Overriding this method for singleton
	return self;
}
@end
