//
//  RegisteredEvent.m
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/28/13.
//
//

#import "RegisteredEvent.h"

@implementation RegisteredEvent
@synthesize title = _title;
@synthesize description = _description;
@synthesize imageUrl = _imageUrl;

-(id)initWithDictionary:(NSDictionary *)aDictionary
{
    self = [super init];
    if (self)
    {
        self.description = [aDictionary objectForKey:@"description"];
        self.imageUrl = [aDictionary objectForKey:@"imageUrl"];
        self.title = [aDictionary objectForKey:@"title"];
        self.occasionArray = (NSArray *)[aDictionary objectForKey:@"occasions"];
    }
    
    return self;
}
- (void)dealloc{
    
    [super dealloc];
    [_title release];
    [_imageUrl release];
    [_description release];
    [_occasionArray release];
}
@end
