//
//  RegisteredEventManagerDelegateProtocol.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/28/13.
//
//

#import <Foundation/Foundation.h>
#import "RegisteredEvent.h"
@protocol RegisteredEventManagerDelegateProtocol <NSObject>
-(void)infoRequestDidFinishForEvent:(NSMutableArray *)ReventList byCancelling:(BOOL)cancelled;
@end
