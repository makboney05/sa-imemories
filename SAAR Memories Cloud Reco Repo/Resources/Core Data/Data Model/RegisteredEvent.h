//
//  RegisteredEvent.h
//  SAAR Memories Cloud Reco
//
//  Created by Boney's Macmini on 5/28/13.
//
//

#import <Foundation/Foundation.h>

@interface RegisteredEvent : NSObject

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *imageUrl;
@property (nonatomic, retain) NSString *description;
@property (nonatomic, retain) NSArray *occasionArray;

-(id)initWithDictionary:(NSDictionary *)aDictionary;
@end
